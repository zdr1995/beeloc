let delete_hive= (id, name)=> {
    $('#hive_id').val(id);
    $("#delete_hive_modal").modal();
    let data= 'Da li ste sigurni da zelite da obrisete ' + name + ' jedinicu';
    $('#delete_hive').html(data);
}

let deleteManyApiars= ()=> {
	let checkboxes= document.getElementsByClassName('hive_ids');
	let ids=[];
	for(let i=0;i<checkboxes.length;i++)
	{
		if(checkboxes[i].checked)
		{
			let id= checkboxes[i].getAttribute('data-id');
			if(id!==undefined)
				ids.push(id);
		}
	}
	if(ids.length>0)
	{
		$('#hive_ids').attr('value',JSON.stringify(ids));
		$('#delete_many_hive_modal').modal();
	}
}