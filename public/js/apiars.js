const BASE_URL='http://172.16.40.70:8000/';
jQuery('document').ready(function($){

    let deleteImg=$('.user-actions__img-delete');
    deleteImg.on( 'click',function() {
        // Id to delete
        let id=$(this).attr('data-id');
        try{
            $.ajax({
                method: "GET",
                dataType: "json",
                timeout:15000,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
                    'Access-Control-Allow-Headers': 'Authorization',
                },
                url:  BASE_URL+'delete/apiar/'+id
                // success: function(response) {
                //     console.log(response);
                // }
                
            }).done(response=> {
                window.location.reload(false);
            });
        }catch(e){
            console.log(e);
        }
       
    });
});