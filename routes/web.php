<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['web',\App\Http\Middleware\Authenticate::class],function(){


    Route::post('notify/moved','Controller@notifyMovedHives');
    Route::post('notify/licensed','Controller@notifyLicenceExpiration');

    Route::get('delete/{resource}/{id}','Controller@destroy');


    // Views Route
    Route::get('dashboard','BaseController@dashboard');

    Route::get('users/list','BaseController@listUsers');
    Route::post('user/search','UserController@searchUsers');
    Route::any('user/list/{id}/{getView}','BaseController@listUserData');
    Route::get('user/add','UserController@addUserView');
    Route::post('user/add','UserController@addUser');
    Route::get('user/edit/{id}','UserController@serveEditView');
    Route::post('user/edit/{id}','UserController@edit');
    Route::post('user/delete','UserController@delete');
    Route::post('user/deleteMany','UserController@deleteMany');

    Route::get('apiars/list/{getView}','BaseController@listApiars');
    Route::post('apiar/search','ApiarController@searchApiars');
    Route::get('apiar/add','ApiarController@apiarAddView');
    Route::post('apiar/add','ApiarController@insertNew');
    Route::post('apiar/delete','ApiarController@delete');
    Route::post('apiar/deleteMany','ApiarController@deleteMany');

    Route::get('apiar/edit/{id}','ApiarController@loadApiarEditView');
    Route::post('apiar/edit/{id}','ApiarController@editApiar');
    Route::post('patch/apiar/{id}','Controller@patch');

    Route::get('/hive/add','HiveController@loadHiveAdd');
    Route::post('hive/search/{apiar_id}','HiveController@searchHive');
    Route::get('hive/add/{apiar_id}','HiveController@loadHiveAddView');
    Route::post('hive/add','HiveController@insert');
    Route::get('hive/edit/{id}','HiveController@loadEditView');
    Route::post('hive/edit/{id}','HiveController@editHive');
    Route::get('hive/{apiarId}','ApiarController@loadHives');
    Route::get('apiar/{id}/hives','HiveController@loadByApiaryId');

    Route::post('/hive/delete','HiveController@delete');
    Route::post('hive/deleteMany','HiveController@deleteMany');

    Route::get('web/logout', 'UserController@logoutWeb');

    Route::get('apiar/sleep/{user_id}/{id}','ApiarController@changeSleepStatus');
});

Route::group(['middleware' => ['jwt.verify']], function(){

    // Route::post('patch/{resource}','BaseController@patch');
});

Route::post('loginWeb','UserController@loginWeb');
Route::get('login',function(){
    return View('login');
})->name('login');

Route::post('login','UserController@loginUser');
Route::get('logout','UserController@logout');
Route::get('loadUser','UserController@getRelationship');



// Route::group(['middleware' => ['jwt.verify']], function() {

// });

Route::get('/', function () {
    return view('login');
});
/*
|----------------------------------------------------------
|   | Main communication route
|----------------------------------------------------------

*/
Route::any('communication','Controller@communicate');


Route::post('create/{resource}','Controller@create');


Route::post('test/distance','HiveController@testDistance');

Route::get('pusher/test','TestController@pusherView');
Route::get('pusher/fire','BaseController@fireEvent');



Route::get('/hive/test','TestController@testHives');
Route::get('/jwt/generate','TestController@jwtGenerate');
Route::post('/license/test','TestController@notifyLicense');


Route::post('regex/test','TestController@testRegex');
Route::post('communicate/test','TestController@communicateTest');
Route::get('log/read','LogController@list');
Route::get('job/create','TestController@createJob');
Route::get('phpinfo','TestController@phpinfo');