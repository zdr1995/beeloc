<?php

use Illuminate\Http\Request;
use App\Http\Middleware\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(Auth::class)->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware'=>Auth::class],function() use($router){
    
    $router->get('user/apiar/cu','BaseController@loadMainCu');
    $router->get('user/apiars','BaseController@loadApiars');
    $router->get('user/status','BaseController@userStatus');
    $router->post('user/token','UserController@storeFCMToken');
    $router->post('user/notify/{id}','UserController@notify');



    $router->get('hive/{apiarId}','Controller@loadHive');

    $router->get('load/{resource}','Controller@loadAll');

    $router->get('load/{resource}/{id}','Controller@loadById');

    $router->post('patch/{resource}/{id}','Controller@patch');


    $router->post('patch/ready_for_listen','UserController@changeListenStatus');


    $router->get('apiar/user',function(){
        $users=\App\User::all();
        $data=[];
        foreach($users as $user)
        {
            $load=$user->with('apiars')->get();
            for($x=0;$x<count($load);$x++)
            {
                $coords=$load[$x];
            }
            // $load->merge($user->apiar->hive)
        }
        return response()->json($load);
    });
});


//Route::post('/login','UserController@loginUser');
//Route::get('/login', 'LoginController@login');

// username:password@www.sajt.com/api/login

// $user = {contains /api/ code} ? request()->user() : Auth::user()
// $user->id
