
{{-- @php(dd($data)) --}}
<form action="{{@$data['route']}}" method="POST">
    @csrf
    <div class="form-row">
        <div class="col">
            @if(Session::has('search')&& Session::get('search')=='hidden')
                @php(Session::flash('search',''))
            @endif
            @if(Session::has('search'))
                <input type="text" class="form-control" placeholder="Pretrazi" name="search" value="{{\session()->get('search')}}">
            @else 
                <input type="text" class="form-control" placeholder="Pretrazi" name="search" value="">
            @endif
        </div>
        <div class="col">
            {{-- @php(dd(Session::has('status'))) --}}
            <select id="inputState" class="form-control" name="status">
                @if(Session::has('status'))
                    @if(\session()->get('status')=='active')
                        <option selected>Status</option>
                        <option value="active" selected>Aktivni</option>
                        <option value="inactive">Neaktivni</option>
                    @endif     
                    @if(\session()->get('status')=='inactive')
                        <option selected>Status</option>
                        <option value="active">Aktivni</option>
                        <option value="inactive" selected>Neaktivni</option>
                    @endif
                    @if(\session()->get('status')=='hidden')
                    @endif
                @else 
                    <option selected>Status</option>
                    <option value="active">Aktivni</option>
                    <option value="inactive">Neaktivni</option>
                @endif
            </select>
        </div>
        @if(isset($data['type']) && $data['type']=='user')
            <div class="col">
                <input type="date" class="form-control" placeholder="date">
            </div>
        @endif
        <div class="col">
            <button type="submit" class="btn btn-primary">Pretrazi</button>
            <form action="{{@$data['route']}}" method="POST">
                @if(isset($data['view']))
                    <input type="text" name="view" value="{{$data['view']}}" hidden>
                @endif
                    {{-- <input type="text" name="search" value="" hidden> --}}
                    <input type="text" name="reset" id="" value="reset" hidden>

                @csrf
                <button class="btn btn-danger" onclick="reset_search()">Resetuj</button>
            </form>
        </div>
    </div>
    @if(isset($data['apiar_id']))
        <input type="text" name="apiar_id" value="{{@$data['apiar_id']}}" hidden>
    @endif
    @if(isset($data['view']))
        {{-- <input type="text" name="view" value="{{$data['view']}}" hidden> --}}
    @endif
</form>

<form id="custom_form">
    <input type="text" name="reset_search" hidden>
</form>