@include('index')
@include('templates/sidebar')

<div class="container">
  <div class="filters row">
  <div class="row my-5 col-md-11">
    <div class="col-12 search-wrapper p-0">

    <?php
      $search_data=[
        'type'=> 'apiar',
        'route'=> '/apiar/search',
        'view' => 'templates/apiar-list'
      ];
    ?>
    @include('templates/search',['data' => $search_data])

    </div>
  </div>
  <div class="row justify-content-end col apiar-add">
    <a href="/apiar/add" class="icon-btn text-primary mr-2" id="apiar_add" style="font-size: 2em;" data-toggle="tooltip" data-placement="bottom" title="Dodaj"><i class="far fa-plus-square"></i></a>
    <button class="icon-btn text-danger" style="font-size: 1.8em;" data-toggle="tooltip" data-placement="bottom" title="Obriši"><i class="far fa-trash-alt" onclick="delete_many_apiars()"></i></button>
  </div>  
  </div>
  <div class="row">
    <table class="table table-hover table-sm">
      <thead>
        <tr>
          <th scope="col">Obriši</th>
          <th scope="col">Naziv</th>
          <th scope="col">Vlasnik</th>
          <th scope="col">Licenca</th>
          <th scope="col">Adresa</th>
          <th scope="col">Baterija</th>
          <th scope="col">Akcije</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($apiars as $key=>$apiar)
        <tr>
          <td style="align-items:center">
            <input type="checkbox" class="mr-3 notify styled-checkbox apiar_ids" data-id="{{$apiar->id}}" id="styled-checkbox-{{$key}}">
            <label for="styled-checkbox-{{$key}}"></label>
          </td>
          <td>{{@$apiar->name}}</td>
          <td>{{@$apiar->users->name}}</td>
          @if(isset($apiar->users))
          {{-- @php(dd($apiar->users->expire)) --}}
              @php($date_expired = new DateTime($apiar->users->expire))
              <td>{{@$date_expired->format('d.m.Y')}}</td>
            @else
                <td></td>
            @endif
          <td>
            @if( isset($apiar->hives[0]) )
              {{@$apiar->hives[0]->address}}
            @endif
          </td>
          <td>
            @if( isset($apiar->hives[0]) )
              {{$apiar->hives[0]->battery}}
              @if( isset($apiar->hives[0]->battery) )
                %
              @endif
            @endif
          </td>
          <td>
            <button class="icon-btn more">
              <a href="/apiar/{{$apiar->id}}/hives">
              <i class="far fa-eye"></i>
              </a>
            </button>
            <a href="/apiar/edit/{{@$apiar->id}}" class="icon-btn edit">
            <i class="fas fa-edit" aria-hidden="true"></i>
            </a>
            {{-- @php(dd(json_encode($apiar->toArray()))) --}}
            <button class="icon-btn text-danger" onclick="change_state({{$apiar->id}},'{{$apiar->name}}')"><i class="fas fa-times"></i></button>
          </td>
        </tr>
        @endforeach

      </tbody>
    </table>
  </div>
</div>



<div class="modal" tabindex="-1" role="dialog" id="delete_apiary_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete">
        <p>Da li ste sigurni da zelite da obrisete Pcelinjak?</p>
      </div>
      <div class="modal-footer">
        <form action="/apiar/delete" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="apiar_id" id="apiar_id" hidden value="">
            <input type="submit" class="btn btn-primary" id="delete_apiary_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="delete_more_apiaries_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete">
        <p>Da li ste sigurni da zelite da obrisete Pcelinjak?</p>
      </div>
      <div class="modal-footer">
        <form action="/apiar/deleteMany" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="apiar_ids" id="apiar_ids" hidden value="">
            <input type="submit" class="btn btn-primary" id="delete_apiary_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script src="{{asset('js/apiar_list.js?time()')}}"></script>

@include('templates/footer')
