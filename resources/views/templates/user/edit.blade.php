@include('../index')
@include('templates/sidebar')

<link rel="stylesheet" href="{{asset('css/templates/user_add.css')}}">
@auth
    
    <div class="container">
        <div class="row my-5">
            <form class="form-user_add" action="/user/edit/{{@$user->id}}" method="POST">
                {{-- <div class="form-row"> --}}
                    <div class="form-group">
                        <label for="inputFullName">Ime</label>
                        <input type="text" class="form-control" placeholder="Full Name" name="name" required value="{{@$user->name}}">
                    </div>
                {{-- </div> --}}
                <div class="form-group">
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="email" class="form-control" placeholder="Email" name="email" required value="{{@$user->email}}">
                        </div>
                    </div>
                <div class="form-group">
                    <label for="inputPhone">Broj telefona</label>
                    <input type="text" class="form-control" placeholder="Phone Number" name="phone_number" value="{{@$user->phone_number}}">
                </div>
                <div class="form-group py-2">
                    <select id="inputRole" class="form-control" name="role" required>
                        <option selected disabled>Role</option>
                        <option value="admin">Admin</option>
                        <option value="user">User</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Password">Licenca važi do</label>
                    <input type="date" class="form-control" name="expire" id="" required value="{{@$user->expire}}">
                </div>
                @csrf
                <div class="form-group row justify-content-lg-end py-2 px-3">
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
@endauth


@include('templates/footer')