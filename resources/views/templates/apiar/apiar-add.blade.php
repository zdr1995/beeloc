@include('../index')
@include('templates/sidebar')

<div class="container">
    <div class="row my-5">
        <form action="/apiar/add" method="POST" class="py-5" style="width:80%;margin-left:10%;">
            <div class="form-group py-2">
                <h4>Novi pcelinjak</h4>
            </div>
            <div class="form-group">
                <label for="nameOfApiary">Ime pcelinjaka</label>
                <input type="text" name="name" placeholder="Ime pcelinjaka" id="" class="form-control">
            </div>
            <div class="form-row py-2">
                <div class="col">
                <label for="nameOfApiary">Ime Centralne jedinice</label>
                <input type="text" name="unit_name" placeholder="Ime Centralne Jedinice" id="" class="form-control">
                </div>
                <div class="col">
                <label for="">IMEI</label>
                <input type="text" name="unit_imei" placeholder="IMEI" id="" class="form-control">
                </div>
            </div>
            <div class="form-group py-2">
                <label for="apiaryAddress">Adresa:</label>
                <input type="text" name="unit_address" placeholder="Adresa Centralne Jedinice" class="form-control">
            </div>
            <div class="form-group py-2">
                <label for="">Korisnik</label>
                <select name="user_id" id="" class="form-control">
                @foreach (\App\User::all() as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
                </select>
            </div>
                @csrf
                {{--  {{ csrf_field() }}  --}}
            <div class="form-group text-right">
                <input type="submit" class="btn btn-primary" value="Potvrdi" />
                <button type="reset" class="btn btn-secondary text-danger" data-dismiss="modal">Ponisti</button>
            </div>
                
        </form>
    </div>
</div>


@include('templates/footer')