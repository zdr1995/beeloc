@include('../index')
@include('templates/sidebar')

<link rel="stylesheet" href="{{asset('css/templates/user_add.css')}}">
@guest
    @php(redirect('login'))
@endguest
@auth
    
    <div class="container">
        <div class="row my-5">
            <form class="form-apiar_edit" action="/apiar/edit/{{@$apiar->id}}" method="POST" style="width:80%;margin-left:10%">
                {{-- <div class="form-row"> --}}
                    <div class="form-group">
                        <label for="inputFullName">Ime pcelinjaka</label>
                        <input type="text" class="form-control" placeholder="Ime pcelinjaka" name="name" required value="{{@$apiar->name}}">
                    </div>
                    @csrf
                {{-- </div> --}}
                <div class="form-group row justify-content-lg-end py-2 px-3">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endauth

@include('templates/footer')