@include('index')
@include('templates/sidebar')
@php(dd($apiar))

<div class="container">
  <div class="row my-5">
    <div class="col-12">
      <form action="">
        <div class="form-row">
          <div class="col-4">
            <input type="text" class="form-control" placeholder="Pretrazi">
          </div>
          <div class="col-2">
            <select id="inputState" class="form-control">
              <option selected>Status...</option>
              <option>...</option>
            </select>
          </div>
          <div class="col-2">
            <input type="date" class="form-control" placeholder="Datum">
          </div>
          <div class="col">
            @csrf
            <button class="btn btn-primary">Pretrazi</button>
            <button class="btn btn-primary">Resetuj</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="row justify-content-end mb-5">
    <a href="apiar/add/{{@$apiar->id}}" class="icon-btn text-primary" id="apiar_add" style="font-size: 2em;"><i class="fas fa-plus-circle"></i></button>
    <button class="icon-btn text-danger" style="font-size: 2em;"><i class="fas fa-trash-alt"></i></button>
  </div>
  <div class="row">
    <table class="table table-hover table-sm">
      <thead>
        <tr>
          <th scope="col">Naziv</th>
          <th scope="col">Vlasnik</th>
          <th scope="col">Licenca</th>
          <th scope="col">Adresa</th>
          <th scope="col">Baterija</th>
          <th scope="col">Akcije</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($apiars as $apiar)
        <tr>
          <td>{{@$apiar->name}}</td>
          <td>{{@$apiar->users->name}}</td>
          <td>{{@$apiar->users->expire}}</td>
          <td>
            @if( isset($apiar->hives[0]) )
              {{$apiar->hives[0]->address}}
            @endif
          </td>
          <td>
            @if( isset($apiar->hives[0]) )
              {{$apiar->hives[0]->battery}}
            @endif
          </td>
          <td>
            <button class="icon-btn text-primary"><i class="fas fa-eye"></i></button>
            <button class="icon-btn text-danger" onclick=change_state({{$apiar->id}})><i class="fas fa-times"></i></button>
          </td>
        </tr>
        @endforeach

      </tbody>
    </table>
  </div>
</div>



@include('templates/footer')