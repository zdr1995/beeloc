@include('index')
@include('templates/sidebar')

<?php 
    #{{-- dd($user->apiars); --}}
    #$hives=$user->hives[0];
    #$hive=$hives[0];
    #{{-- dd($user->apiars); --}}
?>

<link rel="stylesheet" href="{{asset('css/templates/user-details.css')}}">
@if(!isset($user))
    {{redirect('users/list')}}
@else
<div class="container">
    <div class="row my-5">
        
        <div class="col-xs-12 col-md-6 flex-row">
            <div class="card text-white mb-3">
                <div class="card-header text-dark">{{strtoupper($user->name)}}</div>
                <div class="card-body">
                    <table class="table table-borderless table-light">
                        <tbody>
                            <tr>
                                <th scope="col">Adresa:</th><th>Dusanova</th>
                            </tr>
                            <tr>
                                <th>PIB:</th>
                                <td>10510564</td>
                            </tr>
                            <tr>
                                <th>Broj telefona:</th>
                                <td>{{$user->phone_number}}</td>
                            </tr>
                            <tr>
                                <th>Email: </th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>Rola:</th>
                                <td>{{$user->role}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right"><button class="btn btn-danger">Obrisi korisnika</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6">
            <div class="card text-white mb-3">
                @php($date=new DateTime($user->expire))
                <div class="card-header text-dark">
                    STATUS: &nbsp;&nbsp; 
                    @if($user->expire<time())
                        Aktivan do {{$date->format('d.m.Y')}}
                    @else
                        Nije aktivan od {{$date->format('d.m.Y')}}
                    @endif
                </div>
                <div class="card-body">
                    <table class="table table-borderless table-light">
                        <tbody>
                            <tr>
                                <th colspan="2">Zakupljena oprema:</th>
                            </tr>
                            <tr>
                                <th style="color:red">CU:</th>
                                <td style="color:red">{{$user->cu_count}}</td>
                            </tr>
                            <tr>
                                <th>ST:</th>
                                <td>{{$user->st_count}}</td>
                            </tr>
                            <tr>
                                <th>STG: </th>
                                <td>{{$user->stg_count}}</td>
                            </tr>
                            <tr>
                                <th>PT:</th>
                                <td>{{$user->pt_count}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <hr>
                                    {{@$user->unit_number}} 
                                    Jedinica</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        
        <div class="col-xs-12 col-md-6 my-4">
            <table class="table table-hover apiar_list_table table-scroll">
                <thead class="">
                    <tr>
                        <td scope="col" colspan="6">Ukupno pcelinjaka: <b style="color:red">{{$user->apiars->count()}}</b></td>
                        <td scope="col" align="right" class="main_actions">
                            <a href="/apiar/add">
                                <i class="fa fa-plus" aria-hidden="true" style="cursor:pointer"></i>
                            </a>
                                <a href="/hive/add">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->apiars as $apiar)
                        <form action="/user/list/{{$user->id}}/true" id="form-{{$apiar->id}}" method="POST">
                            @csrf
                            <input type="submit" name="apiar_id" value="{{$apiar->id}}" hidden>
                        </form>
                        <tr class="apiar_row" data-attr="{{$apiar->id}}" row_attr="{{$user->id}}" onclick="send_data({{$apiar->id}})">
                            <td scope="row" colspan="6" data-attr="{{$apiar->id}}"  row_attr="{{$user->id}}">{{$apiar->name}}</td>
                            <td scope="col" class="actions" data-attr="{{$apiar->id}}"  row_attr="{{$user->id}}" >
                                <a href="/apiar/sleep/{{$user->id}}/{{$apiar->id}}">
                                    @if($apiar->sleep)
                                        <i class="fa fa-toggle-off on_off text-info" aria-hidden="true"></i>
                                    @else 
                                        <i class="fa fa-toggle-on on_off" aria-hidden="true"></i>
                                    @endif
                                </a>
                                <form action="apiar/sleep/{{$apiar->id}}" id="form-sleep-{{$apiar->id}}">
                                    @csrf
                                    @if($apiar->sleep)
                                        <input type="text" name="sleep" hidden value="0">
                                    @else
                                        <input type="text" name="sleep" hidden value="1">
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-md-6 my-4">
            <table class="table table-hover apiar_single_table" style="overflow:scroll; max-height:200px; overflow-x:hidden;">
                <thead>
                    <tr>
                        @if(isset($user->selected_apiar))
                        <th>{{$user->selected_apiar->hives()->count()}} Drustava</th>
                            <th scope="col" colspan="3">{{$user->selected_apiar->name}}</th>

                            <td scope="col" align="right" class="main_actions">
                                <a href="/hive/add/{{@$user->selected_apiar->id}}">
                                    <i class="fa fa-plus" aria-hidden="true" style="cursor:pointer;" id="add_apiar_btn"></i>
                                </a>
                                <a href="">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        @else
                            <th scope="col" colspan="3">Nije odabran niti jedan pcelinjak...</th>
                        @endif
                        
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Tip</th>
                        <td>Naziv</td>
                        <td>Baterija</td>
                        <td>IMEI</td>
                        <td>Status</td>
                    </tr>
                    <?php
                    //dd($user->selected_apiar)
                    ?>
                    @if(isset($user->selected_apiar))
                        @foreach ($user->selected_apiar->hives as $hive)
                            <tr>
                                <th scope="row">{{$hive->units->type}}</th>
                                <td>{{$hive->name}}</td>
                                <td>{{$hive->battery}}</td>
                                <td>{{$hive->imei}}</td>
                                @if($hive->reporting)
                                    <td class="icons-cell">
                                        <i class="fa fa-times-circle text-danger" aria-hidden="true"></i> 
                                    </td>
                                @else
                                    <td class="icons-cell">
                                        <i class="fa fa-check text-primary" aria-hidden="true"></i> 
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif



<script src="{{asset('js/jquery3.4.1-min.js?time()')}}"></script>
<script src="{{asset('js/user_list.js?time()')}}"></script>