
<div class="col-md-12 col-3 admin-sidebar">
    <div class="admin-logo">
        <img src="{{asset('assets/icons/user.svg')}}" alt="Nema slike" class="admin-image">
        <p class="sidebar-icons_div__text">Admin:</p>
    </div>
    <div class="sidebar-nav">
        <a class="sidebar-icons_div" href="/dashboard">
                <div class="sidebar-icons_placeholder">
                        <img src="{{asset('assets/icons/tachometer.png')}}" alt="" class="sidebar-icons">
                </div>
               
                <div class="sidebar-icons_div__text"><p>pregled</p></div>
        </a>
        <a class="sidebar-icons_div" href="/users/list">
                <div class="sidebar-icons_placeholder">
                        <img src="{{asset('assets/icons/people.png')}}" alt="" class="sidebar-icons">
                </div>
                <div class="sidebar-icons_div__text"><p>korisnici</p></div>
        </a>
        <a class="sidebar-icons_div" href="/apiars/list/true">
                <div class="sidebar-icons_placeholder">
                        <img src="{{asset('assets/icons/apiary.png')}}" alt="" class="sidebar-icons">
                </div>
                <div class="sidebar-icons_div__text"><p>pcelinjaci</p></div>
        </a>
        <a class="sidebar-icons_div">
                <div class="sidebar-icons_placeholder">
                        <img src="{{asset('assets/icons/id-card.png')}}" alt="" class="sidebar-icons">
                </div>
                <div class="sidebar-icons_div__text"><p>licence</p></div>
        </a>
        <a class="sidebar-icons_div settings">
                <div class="sidebar-icons_placeholder">
                        <img src="{{asset('assets/icons/settings.png')}}" alt="" class="sidebar-icons">
                </div>
                <div class="sidebar-icons_div__text"><p>podesavanja</p></div>
        </a>
        <a class="sidebar-icons_div collapse">
            <img src="{{asset('assets/icons/expand-collapse.png')}}" alt="">
        </a>
    </div>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{ asset('js/sidebar.js') }}"></script>
