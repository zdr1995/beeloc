@include('index')
@include('templates/sidebar')
<link rel="stylesheet" href="{{asset('css/data_tables.css')}}">
<link rel="stylesheet" href="{{asset('css/templates/user-list.css')}}">
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> --}}

<div class="container">
<div class="filters row">
  <div class="row my-5 col-md-11">
    <div class="col-12">
    
    <?php
      $search_data=[
        'type' => 'user',
        'route' => '/user/search',
        'view' => 'templates/users-list'
      ];
    ?>
      @include('templates/search', ['data' => $search_data])

    </div>
  </div>

  <div class="row justify-content-end col">
    <a class="icon-btn text-primary mr-2" id="apiar_add" style="font-size: 2em;" data-toggle="tooltip" data-placement="bottom" title="Dodaj" href="/user/add" style="font-size: 2em;"><i class="far fa-plus-square"></i></a>
    <button class="icon-btn text-danger" style="font-size: 1.8em;" data-toggle="tooltip" data-placement="bottom" title="Obriši"><i class="far fa-trash-alt" onclick="deleteManyUsers()"></i></button>
  </div>
    </div>
    <table class="table table-hover table-sm" id="user_list_table">
      <thead>
        <tr>
          <th scope="col">Obriši</th>
          <th scope="col">Ime</th>
          <th scope="col">Email</th>
          <th scope="col">Rola</th>
          <th scope="col">Status</th>
          <th scope="col">Licenca</th>
          <th scope="col">Broj jedinica</th>
          <th scope="col">Akcije</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $key=>$user)
          <tr>
            <td style="align-items:center">
              <input type="checkbox" class="mr-3 notify styled-checkbox user_ids" data-id="{{$user->id}}" id="styled-checkbox-{{$key}}">
              <label for="styled-checkbox-{{$key}}"></label>
            </td>
            <td>{{@$user->name}}</td>
            <td>{{@$user->email}}</td>
            <td>{{@$user->role}}</td>
            <td>
              @if ($user->expire<time())
                Aktivan
              @else
                Neaktivan
              @endif
            </td>
            @php($date = new DateTime($user->expire))
            <td>{{$date->format('d.m.Y')}}</td>
            <td>{{@$user->unit_number}}</td>
            <td style="display:flex">
              <form action="/user/list/{{$user->id}}/true" method="POST">
                @csrf
                <button class="icon-btn more" type="submit"> <i class="far fa-eye"></i></button>
              </form>
              <a href="/user/edit/{{$user->id}}" class="icon-btn edit"> <i class="fas fa-pencil-alt"></i>
              </a>
              <button class="icon-btn text-danger" onclick="pozovi({{$user->id}},'{{$user->name}}')" data-src={{$user->id}}><i class="fas fa-times delete-user__btn" data-src={{$user->id}}></i></button>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
</div>



<div class="modal" tabindex="-1" role="dialog" id="delete_user_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body_user-delete">
        <p>Da li ste sigurni da želite da obrišete korisnika?</p>
      </div>
      <div class="modal-footer">
        <form action="/user/delete" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="id" id="user_id" hidden>
            <input type="submit" class="btn btn-primary" id="delete_user_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>

{{-- Form For deleting more users --}}
<div class="modal" tabindex="-1" role="dialog" id="delete_users_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body_user-delete">
        <p>Da li ste sigurni da želite da obrišete korisnike?</p>
      </div>
      <div class="modal-footer">
        <form action="/user/deleteMany" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="user_ids" id="user_ids" hidden>
            <input type="submit" class="btn btn-primary" id="delete_user_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>



<script src="{{asset('js/users.js?timefs')}}"></script>
@include('templates/footer')
<script src="{{asset('js/data_table.js')}}"></script>
<script>
  $(document).ready(function() {
    $('#user_list_table').DataTable();
  });
</script>

