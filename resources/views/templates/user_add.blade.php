@include('../index')
@include('templates/sidebar')

<link rel="stylesheet" href="{{asset('css/templates/user_add.css')}}">
@auth
    
    <div class="container">
        <div class="row my-5">
            <form class="form-user_add" action="/user/add" method="POST">
                {{-- <div class="form-row"> --}}
                    <div class="form-group">
                        <label for="inputEmail4">Full Name</label>
                        <input type="text" class="form-control" placeholder="Full Name" name="name" required>
                    </div>
                {{-- </div> --}}
                <div class="form-group">
                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="email" class="form-control" placeholder="Email" name="email" required>
                        </div>
                    </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control"placeholder="Password" name="password" required>
                </div>
                <div class="form-group">
                    <label for="inputEmail4">Phone number</label>
                    <input type="text" class="form-control" placeholder="Phone Number" name="phone_number" required>
                </div>
                <div class="form-group py-2">
                    <select id="inputState" class="form-control" name="role" required>
                        <option selected disabled>Role</option>
                        <option value="admin">Admin</option>
                        <option value="user">User</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Password">License up to</label>
                    <input type="date" class="form-control" name="expire" id="" required>
                </div>
                @csrf
                <div class="form-group row justify-content-lg-end py-2 px-3">
                    <button type="submit" class="btn btn-primary">Dodaj korisnika</button>
                </div>
            </form>
        </div>
    </div>
@endauth


@include('templates/footer')