@include('index')
@include('templates/sidebar')

<div class="container">
    <div class="row my-5">
        <form action="/hive/add" method="POST" style="width:80%;margin-left:10%;">
                    <h5 class="modal-title">{{@$apiar->name}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group py-4">
                        <label for="nameOfHive">Ime jedinice</label>
                        <input type="text" name="name" placeholder="Ime jedinice" id="" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="">IMEI</label>
                        <input type="text" name="imei" placeholder="IMEI" id="" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="HiveAddress">Adresa:</label>
                        <input type="number" name="address" placeholder="Adresa:" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="UnitType">Tip jedinice</label>
                        <select name="unit_id" id="" class="form-control">
                        @foreach (\App\Unit::all() as $unit)
                            @if($unit->id!==1)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                    @if(!isset($apiar))
                        <div class="form-group py-2">
                            <label for="ApiarId">Odaberite Pčelinjak</label>
                            <select name="apiar_id" class="form-control">
                                @foreach(\App\Apiar::all() as $apiar)
                                    <option value="{{$apiar->id}}">{{@$apiar->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <input type="text" name="apiar_id" id="apiar_id" value="{{$apiar->id}}" hidden>
                    @endif
                    @csrf
                    <div class="form-group py-5 text-right">
                        <input type="submit" class="btn btn-primary" value="Dodaj jedinicu" />
                        <button type="reset" class="btn btn-secondary text-danger" data-dismiss="modal">Zatvori</button>
                    </div>
                        {{--  {{ csrf_field() }}  --}}
                    </form>
                    </div>
                </div>
                </div>
            </div>
            @csrf
        </form>
    </div>
</div>

@include('templates/footer');