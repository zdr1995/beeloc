@include('index')
@include('templates/sidebar')

<div class="container">
    <div class="row my-5">
        <form action="/hive/edit/{{@$hive->id}}" method="POST" style="width:80%;margin-left:10%;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-group py-4">
                        <label for="nameOfHive">Ime jedinice</label>
                        <input type="text" name="name" placeholder="Ime jedinice" value="{{@$hive->name}}" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="">IMEI</label>
                        <input type="text" name="imei" placeholder="IMEI" value="{{@$hive->imei}}" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="HiveAddress">Adresa:</label>
                        <input type="number" name="address" placeholder="Adresa:" value="{{@$hive->address}}" class="form-control">
                    </div>
                    <div class="form-group py-2">
                        <label for="UnitType">Tip jedinice</label>
                        <select name="unit_id" id="" class="form-control">
                        @foreach (\App\Unit::all() as $unit)
                            @if($unit->id!==@$hive->unit_id)
                                <option value="{{$unit->id}}" selected>{{$unit->name}}</option>
                            @else 
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                    @if(!isset($hive->apiars))
                        <div class="form-group py-2">
                            <label for="ApiarId">Odaberite Pčelinjak</label>
                            <select name="apiar_id" class="form-control">
                                @foreach(\App\Apiar::all() as $apiar)
                                    @if($apiar->id==@$hive->id)
                                        <option value="{{$apiar->id}}" selected>{{@$apiar->name}}</option>
                                    @else 
                                        <option value="{{$apiar->id}}">{{@$apiar->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    @else
                        <input type="text" name="apiar_id" id="apiar_id" value="{{$hive->apiars[0]->id}}" hidden>
                    @endif
                    <div class="form-group py-5 text-right">
                        <input type="submit" class="btn btn-primary" value="Izmeni jedinicu" />
                        <button type="reset" class="btn btn-secondary text-danger" data-dismiss="modal">Zatvori</button>
                    </div>
                        {{--  {{ csrf_field() }}  --}}
                    </form>
                    </div>
                </div>
                </div>
            </div>
            @csrf
        </form>
    </div>
</div>

@include('templates/footer');