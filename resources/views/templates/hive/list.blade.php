@include('index')
@include('templates/sidebar')

<?php 
  $hives= $data['hives'];
  $apiar= $data['apiar'];
?>
<div class="container">
    <div class="row my-5">
      <div class="col-12">

        <?php 
          $search_data=[
            'type'=> 'hive',
            'route'=> '/hive/search/'.@$apiar->id,
            'view'=> '/templates/hive/list',
            'apiar_id'=> @$apiar->id
          ];
        ?>
        @include('templates/search',['data' => $search_data])

      </div>
      <div class="col-md-12 col-xs-12 mt-5">
      <div class="card text-center"  style="height: 100%;">
        <div class="card-body  p-0">
          <div id="map"  style="height:20rem;"></div>
        </div>
      </div>
    </div>
    </div>

    
    <div style="flex-direction: row;display: flex;justify-content: space-between; padding-right: 0.7rem;">
        <div class="">
            <h4>{{@$apiar->name}}</h4>
        </div>
        <div class="row justify-content-end mb-5">
            <a href="/hive/add/{{@$apiar->id}}" id="apiar_add" style="font-size: 2em;" data-toggle="tooltip" data-placement="bottom" title="Dodaj"><i class="far fa-plus-square"></i></a>
            <button class="icon-btn text-danger" style="font-size: 1.8em;" data-toggle="tooltip" data-placement="bottom" title="Obriši"><i class="far fa-trash-alt" onclick="deleteManyApiars()"></i></button>
        </div>
    </div>
    <div class="row">
      
    <div class="col-md-12">
    <table class="table table-hover table-sm">
      
      <thead>
      <tr>
          <th scope="col">Obrisi</th>
          <th scope="col">Tip</th>
          <th scope="col">Naziv</th>
          <th scope="col">long</th>
          <th scope="col">lat</th>
          <th scope="col">Adresa</th>
          <th scope="col">Baterija</th>
          <th scope="col">IMEI</th>
          <th scope="col">Akcije</th>
      </tr>
        </thead>
        <tbody>
        {{-- @dd($hives) --}}
        <script>
          var unitobject= {!! json_encode($hives->toArray()) !!}
        </script>
        @foreach ($hives as $hive)
        {{-- @dd($hive) --}}
          <tr>
              <td style="align-items:center">
                <input type="checkbox" class="mr-3 notify styled-checkbox hive_ids" data-id="{{$hive->id}}" id="styled-checkbox-{{$hive->name}}">
                <label for="styled-checkbox-{{$hive->name}}"></label>
              </td>
              <td>{{@$hive->unit_name}}</td>
              <td>{{@$hive->name}}</td>
              <td>{{@$hive->longitude}}</td>
              <td>{{@$hive->latitude}}</td>
              <td>
                  @if( isset($hive->address) )
                      {{@$hive->address}}
                  @endif
              </td>
              <td style="display:flex;flex-direction:row;vertical-align:middle">
                  @if( isset($hive->battery) )
                      {{@$hive->battery}} <p style="font-size:13px">%</p>
                  @endif
              </td>
              <td>
                  {{@$hive->imei}}
              </td>
              <td>
              <button class="icon-btn text-primary">
                <a href="/hive/edit/{{@$hive->id}}">
                  <i class="fas fa-edit"></i>
                </a>
              </button>
              <button class="icon-btn text-danger" onclick="delete_hive({{$hive->id}},'{{$hive->name}}')"><i class="fas fa-times"></i></button>
              </td>
          </tr>
        @endforeach
        {{-- @dd($hives) --}}

      </tbody>
  </table>
    </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="delete_hive_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete">
        <p id="delete_hive">Da li ste sigurni da želite da obrišete Jedinicu?</p>
      </div>
      <div class="modal-footer">
        <form action="/hive/delete" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="hive_id" id="hive_id" hidden value="">
            <input type="submit" class="btn btn-primary" id="delete_hive_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>

{{-- Delete many hives --}}
<div class="modal" tabindex="-1" role="dialog" id="delete_many_hive_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upozorenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete">
        <p id="delete_hive">Da li ste sigurni da želite da obrišete Jedinice?</p>
      </div>
      <div class="modal-footer">
        <form action="/hive/deleteMany" method="POST">
          @csrf
          {{--  {{ csrf_field() }}  --}}
            <input type="text" name="hive_ids" id="hive_ids" hidden value="">
            <input type="submit" class="btn btn-primary" id="delete_hive_button" value="Potvrdi" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        </form>
      </div>
    </div>
  </div>
</div>


@include('templates/footer')
<script src="{{asset('js/hive/hive_single.js?time()')}}"></script>
<?php //dd($apiar) or die();?>
