@guest
  <script>
    window.location.href='/login';  
  </script>   
@endguest
@auth  
<nav class="sidebar">
    <div class="sidebar-bg"></div>
    <div class="sidebar-overlay"></div>
    <div class="nav">
      <div class="top">
        <a href="/user/edit/{{@\auth()->user()->id}}" class="admin side_item">
          <div class="icon">
            <i class="fas fa-user"></i>
          </div>
          <span>
            {{\auth()->user()->name}}
          </span>
        </a>
        <ul>
          <li>
            <a href="/dashboard" class="side_item" id="/dashboard">
              <div class="icon">
                <i class="fas fa-tachometer-alt"></i>
              </div>
              <span>Pregled</span>
            </a>
          </li>
  
          <li>
            <a href="/apiars/list/true" class="side_item" id="/apiars/list/true">
              <div class="icon">
                <i class="fas fa-archive"></i>
              </div>
              <span>Pčelinjaci</span>
            </a>
          </li>
  
          <li>
            <a href="/users/list" class="side_item" id="/users/list">
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <span>Korisnici</span>
            </a>
          </li>
  
          <li>
            <a href="/log/read" class="side_item" class="" id="/log/read">
              <div class="icon">
                <i class="fa fa-history" aria-hidden="true"></i>
              </div>
              <span>Log fajlovi</span>
            </a>
          </li>
        </ul>
      </div>
      <div class="bottom">
        <a href="/web/logout" class="side_item" id="/settings">
          <div class="icon">
            <i class="fas fa-sign-out-alt"></i>
          </div>
          <span>Izloguj se</span>
        </a>
      </div>
    </div>
</nav>
@endauth