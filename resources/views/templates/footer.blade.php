        </div>

        <script src="{{asset('js/jquery3.4.1-min.js')}}"></script>
        <script src="{{ asset('js/sidebar.js?time()') }}"></script>
        <script src="{{asset('assets/bootstrap-4.3.1-dist/js/bootstrap.js')}}"></script>
        <script src="{{asset('/Font-Awesome/js/all.js')}}"></script>


        <script>
        var map;
        function initMap() {
            let coords= {
                lat: 21.926722689067617,
                lng: 43.1245
            }
            console.log(unitobject);
            if(unitobject!=='undefined' && unitobject[0]!==null) 
            {
                if(unitobject[0] && unitobject[0].longitude!==null && unitobject[0].latitude!==null) 
                {
                    coords= {
                        lat: unitobject[0].latitude,
                        lng: unitobject[0].longitude
                    }
                }
            }
            map = new google.maps.Map(document.getElementById('map'), {
            center: coords,
            zoom: 8
            });
            if(unitobject.length)
            {
                unitobject.forEach(unit=> {
                    let pos= {
                        lat: unit.latitude,
                        lng: unit.longitude
                    }
                    if(pos.lat!==null && pos.lng!==null) {
                        console.log(unit);
                        let marker= new google.maps.Marker({
                            position: pos,
                            map: map,
                            title: unit.name
                        });
                        marker.addListener('click',()=> {
                            window.location.href= '/apiar/'+unit.apiar_id+'/hives';
                        });
                    }
                });
            }
        }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUXGcHmosd9Borm9_H3YEgCGcuue1uqOI&callback=initMap" async defer></script>
        @if(Session::has('message'))
            @if(!Session::has('color'))
                @php($color='green')
            @endif
            <div id="inner-message" class="alert alert-error" style="position:absolute;right:0;bottom:0;background-color:<?php echo \session()->get('color'); ?>">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <p style="color:white">{{\session()->get('message')}}</p>
            </div>
        @endif
    </body>
</html>