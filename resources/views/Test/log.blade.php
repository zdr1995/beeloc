@include('index')
@include('templates/sidebar')
    
<link rel="stylesheet" href="{{asset('css/data_tables.css')}}">
<div class="container my-3">
                
                {{--  MAIN TABLE FOR LOG  --}}
                <table class="table table-hover table-bordered table-sm log-table" id="log_table">

                    {{--  @dd(@$log[4]->report_unit[0])  --}}
                    <thead class="thead">
                        <tr>
                            <th scope="col">Ime jedinice</th>
                            <th scope="col">IMEI</th>
                            <th scope="col">Adresa</th>
                            {{--  <th scope="col">Longituda</th>
                            <th scope="col">Latituda</th>
                            <th scope="col">Amplituda Pomeraja</th>  --}}
                            <th scope="col">Tip</th>
                            <th scope="col">Vreme</th>
                            <th scope="col">Akcije</th>
                        </tr>
                    </thead>
                    @foreach(@$log as $row)
                            {{--  @dd($row->request)  --}}
                            <?php 
                                $time= \Carbon\Carbon::createFromTimeStamp(\strtotime($row->created_at))->diffForHumans()
                            ?>
                            @if(isset($row->hive))
                                <tr class="text">
                                    <td>{{@$row->hive->name}}</th>
                                    <td>{{@$row->hive->imei}}</th>
                                    <td>{{@$row->hive->address}}</td>
                                    {{--  <td>{{@$row->hive->longitude}}</th>
                                    <td>{{@$row->hive->latitude}}</td>
                                    <td>{{@$row->request->Vamp}}</td>  --}}
                                    <td>{{@$row->type}}</td>
                                    <td>{{$time}}</td>
                                    <td class="text-center"><i class="fas fa-eye"></i></td>
                                </tr>
                            @else 
                                {{--  Check Reporting  --}}
                                @if($row->type=='reporting' && isset($row->report_unit))
                                    {{--  @dd($log[4]->report_unit)  --}}
                                    @foreach($row->report_unit as $report)
                                        <?php
                                            $hive=\App\Hive::where('address',$report->address)->where('imei',$row->request->IMEI)->first()
                                        ?>
                                        <tr>
                                            {{--  @dd($row)  --}}
                                            <td>{{@$hive->name}}</td>
                                            <td>{{@$hive->imei}}</td>
                                            <td>{{@$report->address}}</td>
                                            <td>{{@$row->type}}</td>
                                            <td>{{$time}}</td>
                                            <td class="text-center"><i class="fas fa-eye"></i></td>
                                        </tr>
                                    @endforeach
                                @endif

                            @endif
                    @endforeach


                </table>


        </div>

@include('templates/footer')
<script src="{{asset('js/data_table.js')}}" id="log_table_script"></script>
<script>
  $(document).ready(function() {
    $('#log_table').DataTable();
  });
</script>