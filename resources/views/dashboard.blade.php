{{--  Including styles,and meta tags from view html  --}}
@include('index')
@include('templates/sidebar')

@guest
  <script>
    window.location='/login';  
  </script>    
@endguest
@php($expires=\App\User::nearExpiredLicenses())
@php($moved= \App\Hive::movedHives())
<div class="container-fluid">

  <div class="row my-3"> 
    <div class="col-md-10 col-xs-12 map-card">
      <div class="card text-center"  style="height: 100%;">
        <div class="card-body  p-0">
          <div id="map"  style="height: 100%;"></div>
        </div>
      </div>
    </div>
    <div class="col-md-2 col-xs-4 row more-map">
    <div class="col-md-12">
      <div class="card text-center">
        <div class="card-body">
          <h5 class="card-title">Pčelinjaci</h5>
          <h1 class="card-text">{{\App\Apiar::all()->count()}}</h1>
          <a href="/apiar/add" class="btn btn-primary">Dodaj</a>
          {{-- <button class="btn btn-primary" id="add_apiary">Dodaj</button> --}}
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card text-center">
        <div class="card-body">
          <h5 class="card-title">Jedinice</h5>
          <h1 class="card-text">{{ \App\Hive::allUnits() }}</h1>
          <a href="/hive/add" class="btn btn-primary">Dodaj</a>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card text-center">
        <div class="card-body">
          <h5 class="card-title">Korisnici</h5>
          <h1 class="card-text">{{\App\User::all()->count()}}</h1>
          <a href="/user/add" class="btn btn-primary">Dodaj</a>
        </div>
      </div>
    </div>

    </div>
  </div>

  <div class="row">

    <div class="col-md-6 col-xs-12">
      @csrf
        <div class="card pomerene_jedinice">
          <div class="card-header d-flex align-items-center justify-content-between">
            <span>Pomerene jedinice</span>
            <h3 class="text-danger">{{count($moved)}}</h3 class="text-danger">
          </div>
          <div class="card-body">
            <h5>Potrebna akcija</h5>
            {{-- Pass moved units to google maps --}}
            {{-- @php(dd($moved->toArray())) --}}
            <script>
              var unitobject= {!! json_encode($moved->toArray()); !!}
            </script>
            @foreach ($moved as $key=>$moved_unit)
              <div class="d-flex justify-content-between align-items-center">
                <label class="d-flex align-items-center my-0">

                  <input type="checkbox" class="mr-3 notify styled-checkbox" id="styled-checkbox-{{$key}}-action" name="hive_id" value={{$moved_unit->id}}>
                  <label for="styled-checkbox-{{$key}}-action">{{$moved_unit->name}}</label>
                </label>
                <div>
                  @if ($moved_unit->reporting)
                    <span class="icon-btn text-success mx-1">
                      <i class="fas fa-link"></i>
                    </span>
                  @else
                    <span class="icon-btn text-danger mx-1">
                      <i class="fas fa-unlink"></i>
                    </span>
                  @endif
                  @if (intval($moved_unit->battery)>50)
                    <span class="icon-btn text-success mx-1">
                      <a href="">
                        <i class="fas fa-battery-full"></i>
                      </a>
                    </span>
                  @else
                    <span class="icon-btn text-danger mx-1">
                      <i class="fas fa-battery-quarter"></i>
                    </span>
                  @endif
                </div>
              </div>
            @endforeach
              
            </div>
              <div class="card-footer d-flex justify-content-end">
                <button class="btn btn-primary" id="notify_button">
                  Obavesti
                </button>
              {{--  <input type="submit" name="submit" class="btn btn-primary" id="" value="Obavesti">  --}}
            </div>
        </div>
      </div>


      <div class="col-md-6 col-xs-12">
        <div class="card aktivne_licence">
          <div class="card-header d-flex align-items-center justify-content-between">
            <span>Aktivne licence</span>
            <h3>6</h3>
          </div>
          <div class="card-body">
            <h5>Pred istekom</h5>
  
            @foreach ($expires as $key => $user)
              @php($date = new DateTime($user->expire))              
              <div class="d-flex justify-content-between align-items-center aktivne_licence_item">
                <label class="d-flex align-items-center my-0">
                  <input type="checkbox" class="mr-3 licence_notify styled-checkbox" id="styled-checkbox-{{$key}}">
                  <label for="styled-checkbox-{{$key}}">{{$user->name}}</label>
                </label>
                <div class="date">
                  <span>{{$date->format('d.m.Y')}}</span>
                </div>
                <div class="more">
                  <a href="/user/list/{{@$user->id}}/true">
                    <i class="fas fa-eye"></i>
                  </a>
                </div>
              </div>
            @endforeach
  
          </div>
          <div class="card-footer d-flex justify-content-end">
            <button class="btn btn-primary" id="notify_license">
              Obavesti
            </button>
          </div>
        </div>
      </div>

    </div>
</div>


{{-- ADD APIARY MODAL --}} 
{{-- <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form> --}}



<form action="/apiar/add" method="POST">
  @csrf
  <div class="modal" tabindex="-1" role="dialog" id="add_apiary_modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Dodavanje pčelinjaka</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nameOfApiary">Ime pčelinjaka</label>
            <input type="text" name="name" placeholder="Ime pcelinjaka" id="" class="form-control">
          </div>
          <div class="form-row py-2">
            <div class="col">
              <label for="nameOfApiary">Ime Centralne jedinice</label>
              <input type="text" name="unit_name" placeholder="Ime Centralne Jedinice" id="" class="form-control">
            </div>
            <div class="col">
              <label for="">Adresa</label>
              <input type="text" name="unit_address" placeholder="Adresa" id="" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="nameOfApiary">Imei</label>
            <input type="text" name="unit_imei" placeholder="IMEI" id="" class="form-control">
          </div>
          <div class="form-group py-2">
            <label for="">Korisnik</label>
            <select name="user_id" id="" class="form-control">
              @foreach (\App\User::all() as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <form action="/user/delete" method="POST">
            @csrf
            {{--  {{ csrf_field() }}  --}}
              <input type="text" name="id" id="user_id" hidden>
              <input type="submit" class="btn btn-primary" id="delete_user_button" value="Potvrdi" />
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</form>



<script type="text/javascript" src="{{asset('js/dashboard.js?time=2')}}"></script>
@include('templates/footer')
