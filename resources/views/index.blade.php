<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{asset('/assets/bootstrap-4.3.1-dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('/Font-Awesome/css/all.css')}}">  
  <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
  <link rel="stylesheet" href="{{asset('/css/app.css')}}">

  <title>BeeLoc</title>
</head>
<body>

    <div style="margin-left: 100px;">
