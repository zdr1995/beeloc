@auth
<script>
	window.location.href='/dashboard';
</script>
@endauth

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="{{ asset('/css/style.css') }}">
<link rel="stylesheet" href="{{asset('/css/app.css')}}">
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html><html lang='en' class=''>
<head>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css'><script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
</head><body>
<div class="login" style="background-image: url('{{asset('/img/bg-1.jpg')}}');">
	<div class="login-content">
	<img src="{{asset('/img/logo.svg')}}" alt="">
	<h1>Uloguj se</h1>
    <form method="POST" action="/loginWeb">
    	<input type="text" placeholder="Korisnicko ime" required="required" name="email"/>
        <input type="password" placeholder="Lozinka" required="required" name="password"/>
        <button type="submit" class="btn btn-primary btn-block btn-large">Potvrdi</button>
    </form>
	</div>
</div>
<!-- <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script> -->
<script >/* 

I built this login form to block the front end of most of my freelance wordpress projects during the development stage. 

This is just the HTML / CSS of it but it uses wordpress's login system. 

Nice and Simple

*/
//# sourceURL=pen.js
</script>
</body></html>