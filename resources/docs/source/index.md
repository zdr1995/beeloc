---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_cb813a53aefe80bce2ec72997a6ef472 -->
## api/user/apiar/cu
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/apiar/cu" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/apiar/cu"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/user/apiar/cu`


<!-- END_cb813a53aefe80bce2ec72997a6ef472 -->

<!-- START_6ebf8acde4d26a3495980568ef20ed32 -->
## api/user/apiars
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/apiars" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/apiars"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/user/apiars`


<!-- END_6ebf8acde4d26a3495980568ef20ed32 -->

<!-- START_a67275d765dbeb96a45a03c889c7ad25 -->
## api/hive/{apiarId}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/hive/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/hive/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/hive/{apiarId}`


<!-- END_a67275d765dbeb96a45a03c889c7ad25 -->

<!-- START_39137fbd6c8678a1c482940b1f100440 -->
## api/user/status
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/status" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/status"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/user/status`


<!-- END_39137fbd6c8678a1c482940b1f100440 -->

<!-- START_2e01ddd02b282b84dbe4a0d5d6e5750e -->
## api/user/token
> Example request:

```bash
curl -X POST \
    "http://localhost/api/user/token" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/token"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/user/token`


<!-- END_2e01ddd02b282b84dbe4a0d5d6e5750e -->

<!-- START_b6aa0d2b5164874665fd42084aa243b4 -->
## api/load/{resource}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/load/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/load/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/load/{resource}`


<!-- END_b6aa0d2b5164874665fd42084aa243b4 -->

<!-- START_590e6127c5b9f83331e9a3e5c532996a -->
## api/load/{resource}/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/load/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/load/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
"Not allowed"
```

### HTTP Request
`GET api/load/{resource}/{id}`


<!-- END_590e6127c5b9f83331e9a3e5c532996a -->

<!-- START_6bd0e4711eb86e86bb32ef98b6cbdc32 -->
## api/patch/{resource}/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/api/patch/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/patch/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/patch/{resource}/{id}`


<!-- END_6bd0e4711eb86e86bb32ef98b6cbdc32 -->

<!-- START_bd76639d3b85e332dc5289c392416f99 -->
## api/user/notify/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/api/user/notify/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/notify/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/user/notify/{id}`


<!-- END_bd76639d3b85e332dc5289c392416f99 -->

<!-- START_e9609311b9b30193b4f8f1a981dfdb70 -->
## api/patch/ready_for_listen
> Example request:

```bash
curl -X POST \
    "http://localhost/api/patch/ready_for_listen" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/patch/ready_for_listen"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/patch/ready_for_listen`


<!-- END_e9609311b9b30193b4f8f1a981dfdb70 -->

<!-- START_6ed65ea7b8bf0eabd45b740abc2a4941 -->
## notify/moved
> Example request:

```bash
curl -X POST \
    "http://localhost/notify/moved" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/notify/moved"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST notify/moved`


<!-- END_6ed65ea7b8bf0eabd45b740abc2a4941 -->

<!-- START_04c9102d3d851c3cda9083b61a6095a4 -->
## delete/{resource}/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/delete/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/delete/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET delete/{resource}/{id}`


<!-- END_04c9102d3d851c3cda9083b61a6095a4 -->

<!-- START_745961922bf96196a22455853aa5abfe -->
## user/delete
> Example request:

```bash
curl -X POST \
    "http://localhost/user/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST user/delete`


<!-- END_745961922bf96196a22455853aa5abfe -->

<!-- START_30059a09ef3f0284c40e4d06962ce08d -->
## dashboard
> Example request:

```bash
curl -X GET \
    -G "http://localhost/dashboard" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/dashboard"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET dashboard`


<!-- END_30059a09ef3f0284c40e4d06962ce08d -->

<!-- START_b13750c049a32516a5b748892b699b5c -->
## users/list
> Example request:

```bash
curl -X GET \
    -G "http://localhost/users/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/users/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET users/list`


<!-- END_b13750c049a32516a5b748892b699b5c -->

<!-- START_e0fbde0e6635672df5f0132d1d7bdfdd -->
## user/search
> Example request:

```bash
curl -X POST \
    "http://localhost/user/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST user/search`


<!-- END_e0fbde0e6635672df5f0132d1d7bdfdd -->

<!-- START_eefbd64f0e050c922b423c966a9f0f09 -->
## user/list/{id}/{getView}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/user/list/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/list/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET user/list/{id}/{getView}`

`POST user/list/{id}/{getView}`

`PUT user/list/{id}/{getView}`

`PATCH user/list/{id}/{getView}`

`DELETE user/list/{id}/{getView}`

`OPTIONS user/list/{id}/{getView}`


<!-- END_eefbd64f0e050c922b423c966a9f0f09 -->

<!-- START_27b59273bf1cc77a84f2e229fd7af02d -->
## user/add
> Example request:

```bash
curl -X GET \
    -G "http://localhost/user/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET user/add`


<!-- END_27b59273bf1cc77a84f2e229fd7af02d -->

<!-- START_e70255a0d44fc816826dc2d6c090dd27 -->
## user/add
> Example request:

```bash
curl -X POST \
    "http://localhost/user/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST user/add`


<!-- END_e70255a0d44fc816826dc2d6c090dd27 -->

<!-- START_28e29a853f958df581bb90f39203a702 -->
## user/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/user/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET user/edit/{id}`


<!-- END_28e29a853f958df581bb90f39203a702 -->

<!-- START_6233ec8047a58e6b3486912f72f36f8c -->
## user/edit/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/user/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST user/edit/{id}`


<!-- END_6233ec8047a58e6b3486912f72f36f8c -->

<!-- START_9a031c83e89a02c3fcaeeec4505490b7 -->
## apiars/list/{getView}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/apiars/list/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiars/list/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET apiars/list/{getView}`


<!-- END_9a031c83e89a02c3fcaeeec4505490b7 -->

<!-- START_f89d6417623eb4d89350b7a6da33b6cc -->
## apiar/search
> Example request:

```bash
curl -X POST \
    "http://localhost/apiar/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST apiar/search`


<!-- END_f89d6417623eb4d89350b7a6da33b6cc -->

<!-- START_3d4989ca496309ac9a2b26928e3ce7d5 -->
## apiar/add
> Example request:

```bash
curl -X GET \
    -G "http://localhost/apiar/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET apiar/add`


<!-- END_3d4989ca496309ac9a2b26928e3ce7d5 -->

<!-- START_bac78dbd4c5d8721522117c08675f61e -->
## apiar/add
> Example request:

```bash
curl -X POST \
    "http://localhost/apiar/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST apiar/add`


<!-- END_bac78dbd4c5d8721522117c08675f61e -->

<!-- START_e06ecbb8fda077b0efee1b9da0cb80da -->
## apiar/delete
> Example request:

```bash
curl -X POST \
    "http://localhost/apiar/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST apiar/delete`


<!-- END_e06ecbb8fda077b0efee1b9da0cb80da -->

<!-- START_5d145ecba7076b0cf8f5a5fa7395451c -->
## apiar/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/apiar/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET apiar/edit/{id}`


<!-- END_5d145ecba7076b0cf8f5a5fa7395451c -->

<!-- START_e6ec2e685afbecc3ea49a3460ddeb899 -->
## apiar/edit/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/apiar/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST apiar/edit/{id}`


<!-- END_e6ec2e685afbecc3ea49a3460ddeb899 -->

<!-- START_676bf8af49a58556802870cb9936609b -->
## patch/apiar/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/patch/apiar/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/patch/apiar/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST patch/apiar/{id}`


<!-- END_676bf8af49a58556802870cb9936609b -->

<!-- START_16275c50e2a88362d518e1c671fa3962 -->
## hive/add
> Example request:

```bash
curl -X GET \
    -G "http://localhost/hive/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET hive/add`


<!-- END_16275c50e2a88362d518e1c671fa3962 -->

<!-- START_af507a10e0bc9c3c4d12f4859c7e4675 -->
## hive/search/{apiar_id}
> Example request:

```bash
curl -X POST \
    "http://localhost/hive/search/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/search/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST hive/search/{apiar_id}`


<!-- END_af507a10e0bc9c3c4d12f4859c7e4675 -->

<!-- START_93ec2c931ce8367bf60678371d40871a -->
## hive/add/{apiar_id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/hive/add/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/add/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET hive/add/{apiar_id}`


<!-- END_93ec2c931ce8367bf60678371d40871a -->

<!-- START_82858004366e77ee5eaa9e1fd1b97af5 -->
## hive/add
> Example request:

```bash
curl -X POST \
    "http://localhost/hive/add" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/add"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST hive/add`


<!-- END_82858004366e77ee5eaa9e1fd1b97af5 -->

<!-- START_3e91591fccb7f34ed55ab203777ec61f -->
## hive/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/hive/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET hive/edit/{id}`


<!-- END_3e91591fccb7f34ed55ab203777ec61f -->

<!-- START_c146310acdac22eb01e66913e2047dfa -->
## hive/edit/{id}
> Example request:

```bash
curl -X POST \
    "http://localhost/hive/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST hive/edit/{id}`


<!-- END_c146310acdac22eb01e66913e2047dfa -->

<!-- START_607268014bee6c105647a1534f7495f2 -->
## hive/{apiarId}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/hive/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET hive/{apiarId}`


<!-- END_607268014bee6c105647a1534f7495f2 -->

<!-- START_1fd32d3c3c86f7add16f426369bfe30f -->
## apiar/{id}/hives
> Example request:

```bash
curl -X GET \
    -G "http://localhost/apiar/1/hives" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/1/hives"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET apiar/{id}/hives`


<!-- END_1fd32d3c3c86f7add16f426369bfe30f -->

<!-- START_9d0b39ff2bb8d9f93624cfaf5a5062af -->
## hive/delete
> Example request:

```bash
curl -X POST \
    "http://localhost/hive/delete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/delete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST hive/delete`


<!-- END_9d0b39ff2bb8d9f93624cfaf5a5062af -->

<!-- START_dbe43b26df180eafdd350b7280b7c7a9 -->
## web/logout
> Example request:

```bash
curl -X GET \
    -G "http://localhost/web/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/web/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET web/logout`


<!-- END_dbe43b26df180eafdd350b7280b7c7a9 -->

<!-- START_cd4b3316849b12247cf70afe8cca3254 -->
## apiar/sleep/{user_id}/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/apiar/sleep/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/apiar/sleep/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET apiar/sleep/{user_id}/{id}`


<!-- END_cd4b3316849b12247cf70afe8cca3254 -->

<!-- START_ba3f4395281202f328c7d3105c4c428a -->
## loginWeb
> Example request:

```bash
curl -X POST \
    "http://localhost/loginWeb" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/loginWeb"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST loginWeb`


<!-- END_ba3f4395281202f328c7d3105c4c428a -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## login
> Example request:

```bash
curl -X POST \
    "http://localhost/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_568bd749946744d2753eaad6cfad5db6 -->
## logout
> Example request:

```bash
curl -X GET \
    -G "http://localhost/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET logout`


<!-- END_568bd749946744d2753eaad6cfad5db6 -->

<!-- START_9695df9753b199513622ed0b7fe66f3e -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/loadUser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/loadUser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    [
        {
            "id": 1,
            "name": "Djordje Petrovic",
            "email": "djordje.petrovic@itcentar.rs",
            "email_verified_at": null,
            "phone_number": "064-414-6866",
            "expire": "2032-01-01",
            "status": 1,
            "role": "admin",
            "access_token": null,
            "fcm_token": "fwKh9X6oT50:APA91bFw5bYyUn-oYBR6cf8Gc1oLHRORmIQzyD4YbNVVOSdE8-9LmpEYwYV1QMxCkRzJscUd-MwmBlRPP8lbbaLNrUgKyzjuFEkYFZ4EMP2ejCEUWxuAwfyCs-tlaE96a7bQ478qn_rh",
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 2,
            "name": "Zdravko Sokcevic",
            "email": "zdravko.sokcevic@itcentar.rs",
            "email_verified_at": null,
            "phone_number": null,
            "expire": "2032-01-01",
            "status": 1,
            "role": "admin",
            "access_token": null,
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 3,
            "name": "Test Korisnik",
            "email": "test@test.com",
            "email_verified_at": "2019-08-28 12:44:04",
            "phone_number": null,
            "expire": "2019-12-31",
            "status": 1,
            "role": "admin",
            "access_token": "VuX2Gzoyg2eY4hlhbn40EkF68",
            "fcm_token": "e-ciU5LQVk4:APA91bHu-6djiDYpQPjhJ66tWsrB8NrllM_yDyRiLvpLhPkbF3lSTS3-tNpBPz1uU5geFkwNcTwJbdCUa_-9wuGK8W9PRPEHy8VOrCOZ-J5EhwYdx8ljEtOBuZTiez5g_vv3npmYn7su",
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 13,
            "name": "Mr. Randy Lind",
            "email": "vhowe@example.net",
            "email_verified_at": "2019-08-28 12:44:04",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 0,
            "role": "admin",
            "access_token": "SxVLrReAQH4VGhVrpqdz4hulW",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 19,
            "name": "Gonzalo Krajcik DDS",
            "email": "hohara@example.net",
            "email_verified_at": "2019-08-28 12:44:05",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 0,
            "role": "admin",
            "access_token": "ZSTRYt3DIWA5bj6A91xfFi2zz",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 22,
            "name": "Prof. Alexandra Russel",
            "email": "pearl23@example.org",
            "email_verified_at": "2019-08-28 12:44:05",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 1,
            "role": "admin",
            "access_token": "VdpKQ6llWrQN1eY2P60xYB1ma",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 27,
            "name": "Mireya Swaniawski",
            "email": "dannie37@example.net",
            "email_verified_at": "2019-08-28 12:44:05",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 1,
            "role": "admin",
            "access_token": "3e8rE06tLSzaBom87xl7yilPd",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 28,
            "name": "Candice Kling",
            "email": "lehner.tad@example.org",
            "email_verified_at": "2019-08-28 12:44:05",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 0,
            "role": "admin",
            "access_token": "R6OQFkxM0uyCIKUQquppyqGRF",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 31,
            "name": "Patsy Weissnat",
            "email": "ondricka.vance@example.com",
            "email_verified_at": "2019-08-28 12:44:05",
            "phone_number": null,
            "expire": "2020-11-28",
            "status": 1,
            "role": "admin",
            "access_token": "ThwB6E6Jo8SMAONAJHknwoDTy",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 54,
            "name": "Milos Paunovic",
            "email": "milos.paunovic@itcentar.rs",
            "email_verified_at": "2019-08-28 12:44:07",
            "phone_number": "00381641486214",
            "expire": "2020-11-28",
            "status": 0,
            "role": "admin",
            "access_token": "go3viDsn0litOj142N21e8GCZ",
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 69,
            "name": "Petko Petkovic",
            "email": "petko@mail.com",
            "email_verified_at": null,
            "phone_number": null,
            "expire": "2019-12-11",
            "status": 1,
            "role": "admin",
            "access_token": null,
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 71,
            "name": "Janko Jankovic",
            "email": "janko@mail.com",
            "email_verified_at": null,
            "phone_number": null,
            "expire": "2019-12-26",
            "status": 0,
            "role": "admin",
            "access_token": null,
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 72,
            "name": "Milos Paunovic",
            "email": "milos@mail.com",
            "email_verified_at": null,
            "phone_number": null,
            "expire": "2019-12-10",
            "status": 1,
            "role": "admin",
            "access_token": null,
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ],
    [
        {
            "id": 75,
            "name": "Pavle Vukovic",
            "email": "pavle.vukovic@itcentar.rs",
            "email_verified_at": null,
            "phone_number": "065144624",
            "expire": "2020-01-16",
            "status": 0,
            "role": "admin",
            "access_token": null,
            "fcm_token": null,
            "ready_for_listen": null
        },
        null
    ]
]
```

### HTTP Request
`GET loadUser`


<!-- END_9695df9753b199513622ed0b7fe66f3e -->

<!-- START_e250a432c3348a594cb67f37bda4bf2e -->
## communication
> Example request:

```bash
curl -X GET \
    -G "http://localhost/communication" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/communication"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
{
    "message": "not found"
}
```

### HTTP Request
`GET communication`

`POST communication`

`PUT communication`

`PATCH communication`

`DELETE communication`

`OPTIONS communication`


<!-- END_e250a432c3348a594cb67f37bda4bf2e -->

<!-- START_bf3b18f6a93050ab75e563133bde4474 -->
## create/{resource}
> Example request:

```bash
curl -X POST \
    "http://localhost/create/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/create/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST create/{resource}`


<!-- END_bf3b18f6a93050ab75e563133bde4474 -->

<!-- START_732dfe7d51da42ebd3bcda3b2cb4f556 -->
## test/distance
> Example request:

```bash
curl -X POST \
    "http://localhost/test/distance" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/test/distance"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST test/distance`


<!-- END_732dfe7d51da42ebd3bcda3b2cb4f556 -->

<!-- START_571d9059353d4c8f012034ca04d39cb8 -->
## pusher/test
> Example request:

```bash
curl -X GET \
    -G "http://localhost/pusher/test" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/pusher/test"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET pusher/test`


<!-- END_571d9059353d4c8f012034ca04d39cb8 -->

<!-- START_cdc52cb10adda1e1c9be3520b670389c -->
## pusher/fire
> Example request:

```bash
curl -X GET \
    -G "http://localhost/pusher/fire" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/pusher/fire"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "event fired"
}
```

### HTTP Request
`GET pusher/fire`


<!-- END_cdc52cb10adda1e1c9be3520b670389c -->

<!-- START_94273a16699a808f7478fca736899f7c -->
## hive/test
> Example request:

```bash
curl -X GET \
    -G "http://localhost/hive/test" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/hive/test"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET hive/test`


<!-- END_94273a16699a808f7478fca736899f7c -->

<!-- START_4f00eb1ffefea9d3214333bbc23b8f11 -->
## jwt/generate
> Example request:

```bash
curl -X GET \
    -G "http://localhost/jwt/generate" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/jwt/generate"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET jwt/generate`


<!-- END_4f00eb1ffefea9d3214333bbc23b8f11 -->

<!-- START_5bfe03b9f909a98ce007cf5a045ce643 -->
## license/test
> Example request:

```bash
curl -X POST \
    "http://localhost/license/test" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/license/test"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST license/test`


<!-- END_5bfe03b9f909a98ce007cf5a045ce643 -->

<!-- START_406240b15031a604fb753f3f8a4294a9 -->
## regex/test
> Example request:

```bash
curl -X POST \
    "http://localhost/regex/test" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/regex/test"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST regex/test`


<!-- END_406240b15031a604fb753f3f8a4294a9 -->

<!-- START_096ff769e7a0be269587c330c760976a -->
## communicate/test
> Example request:

```bash
curl -X POST \
    "http://localhost/communicate/test" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/communicate/test"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST communicate/test`


<!-- END_096ff769e7a0be269587c330c760976a -->

<!-- START_5254426c448412ed6d5931636bc16364 -->
## log/read
> Example request:

```bash
curl -X GET \
    -G "http://localhost/log/read" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/log/read"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET log/read`


<!-- END_5254426c448412ed6d5931636bc16364 -->

<!-- START_6a249518193465cd851009fb3d89941f -->
## job/create
> Example request:

```bash
curl -X GET \
    -G "http://localhost/job/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/job/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET job/create`


<!-- END_6a249518193465cd851009fb3d89941f -->

<!-- START_66df3678904adde969490f2278b8f47f -->
## Authenticate the request for channel access.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/broadcasting/auth" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/broadcasting/auth"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET broadcasting/auth`

`POST broadcasting/auth`


<!-- END_66df3678904adde969490f2278b8f47f -->


