-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 10, 2020 at 06:20 PM
-- Server version: 8.0.18-0ubuntu0.19.10.1
-- PHP Version: 7.3.11-0ubuntu0.19.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hive`
--

-- --------------------------------------------------------

--
-- Table structure for table `apiars`
--

CREATE TABLE `apiars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sleep` int(11) NOT NULL DEFAULT '0',
  `change` int(11) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apiars`
--

INSERT INTO `apiars` (`id`, `name`, `sleep`, `change`, `user_id`) VALUES
(7, 'Pcelinjak 1', 0, 1, 1),
(8, 'Pcelinjak 2', 1, 0, 3),
(10, 'Dayne Pollich', 0, 0, 4),
(19, 'Prof. Casimir Maggio Sr.', 0, 0, 1),
(25, 'Carlotta Lehner Jr.', 0, 0, 19),
(28, 'Julie Baumbach', 0, 0, 22),
(33, 'Pascale Hettinger IV', 1, 1, 27),
(34, 'Jody Grant III', 1, 1, 28),
(37, 'Ms. Maryjane Zboncak MD', 1, 0, 31),
(47, 'Hope Crooks', 1, 0, 41),
(54, 'Llewellyn Barrows', 0, 0, 48),
(78, 'Pcelinjak 62', 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `hives`
--

CREATE TABLE `hives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `reporting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `battery` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification` int(11) DEFAULT NULL,
  `last_reporting` date NOT NULL,
  `imei` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apiar_id` bigint(20) UNSIGNED NOT NULL,
  `unit_id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hives`
--

INSERT INTO `hives` (`id`, `name`, `longitude`, `latitude`, `reporting`, `main`, `battery`, `notification`, `last_reporting`, `imei`, `apiar_id`, `unit_id`, `address`) VALUES
(8, 'Centralna jedinica', 21.926722689067617, 42.54730363019271, '0', '1', '75', 1, '2019-08-28', '616523505056684', 7, 1, '10'),
(9, 'Sensor Tag', 43.6251970656641, 20.89513800262705, '0', '0', '86', 0, '2019-08-28', '6', 7, 2, '10'),
(10, 'Sensor Tag + GPS', 43.6251970656641, 20.89513800262705, '1', '0', '91', 1, '2019-08-28', '7', 7, 3, '10'),
(11, 'Central Unit 1', 21.71095111880416, 43.53991946788234, '1', '1', '80', 1, '2019-08-28', '724380962590920', 8, 1, '10'),
(12, 'Sensor Tag', 43.6251970656641, 20.89513800262705, '1', '0', '33', 1, '2019-08-28', '4', 8, 2, '5'),
(13, 'ST + GPS', 43.6251970656641, 20.89513800262705, '1', '0', '61', 0, '2019-08-28', '8', 8, 3, '12846641168'),
(14, 'Potera Unit', 20.89513800262705, 43.6251970656641, '1', '0', '68', 1, '2019-08-28', '399229957088234', 8, 4, '12846641168'),
(26, 'Narciso Dach', 43.6251970656641, 20.89513800262705, '1', '1', '46', 0, '2019-08-28', '003888851492715', 25, 1, '12846641168'),
(29, 'Mallie Brekke', 43.6251970656641, 20.89513800262705, '0', '0', '41', 1, '2019-08-28', '471183136640269', 28, 1, '12846641168'),
(34, 'Aniyah Wolff MD', 43.6251970656641, 20.89513800262705, '1', '1', '49', 0, '2019-08-28', '052352923872867', 33, 3, '12846641168'),
(35, 'Donny Price', 43.6251970656641, 20.89513800262705, '0', '1', '30', 0, '2019-08-28', '318106382530329', 34, 2, '12846641168'),
(38, 'Maxie Marquardt', 43.6251970656641, 20.89513800262705, '0', '1', '53', 0, '2019-08-28', '089323157119405', 37, 3, '12846641168'),
(48, 'Mrs. Serena Von I', 43.6251970656641, 20.89513800262705, '0', '1', '72', 1, '2019-08-28', '820097575332386', 47, 4, '12846641168'),
(70, 'CU 62', NULL, NULL, NULL, '1', NULL, NULL, '2020-01-09', '1111995101475', 78, 1, '10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_08_12_082435_apiar_table_migration', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(7, '2016_06_01_000004_create_oauth_clients_table', 1),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2019_08_10_152121_unit_table_migration', 1),
(10, '2020_08_09_152130_hive_table_migration', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('ST','CU','ST+GPS','PU') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `type`) VALUES
(1, 'Central Unit', 'CU'),
(2, 'Sensor Tag', 'ST'),
(3, 'Sensor Tag + GPS', 'ST+GPS'),
(4, 'Potera Unit', 'PU');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expire` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fcm_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone_number`, `expire`, `status`, `role`, `remember_token`, `access_token`, `created_at`, `updated_at`, `fcm_token`) VALUES
(1, 'Djordje Petrovic', 'djordje.petrovic@itcentar.rs', NULL, '$2y$10$/trXu.le0tKYkQ/hJzbh3eDIjkAjoSFywZ7ToWWdjdtnJE9yqFKzy', '064-414-6866', '2032-01-01', 1, 'admin', NULL, NULL, NULL, NULL, 'fwKh9X6oT50:APA91bFw5bYyUn-oYBR6cf8Gc1oLHRORmIQzyD4YbNVVOSdE8-9LmpEYwYV1QMxCkRzJscUd-MwmBlRPP8lbbaLNrUgKyzjuFEkYFZ4EMP2ejCEUWxuAwfyCs-tlaE96a7bQ478qn_rh'),
(2, 'Zdravko Sokcevic', 'zdravko.sokcevic@itcentar.rs', NULL, '$2y$10$97AlHFbegkqq/jTmJw0O2OACLbMq8F6OqL.9RnD3cyphuV8hbSzBy', NULL, '2032-01-01', 1, 'admin', NULL, NULL, NULL, NULL, NULL),
(3, 'Test Korisnik', 'test@test.com', '2019-08-28 10:44:04', '$2y$12$SLg/zAMzptqe8yWmjqne9OxtheWhp3hxadtjZCZmp.MkghVJfk7wO', NULL, '2019-12-31', 1, 'admin', 'FRxdv1uNhrPuPn0PVyVPXPeXGAoNEF1Z4OhFrh5t6agFoM2HMclgCES3LS2Z', 'VuX2Gzoyg2eY4hlhbn40EkF68', NULL, '2019-09-06 07:06:56', 'e-ciU5LQVk4:APA91bHu-6djiDYpQPjhJ66tWsrB8NrllM_yDyRiLvpLhPkbF3lSTS3-tNpBPz1uU5geFkwNcTwJbdCUa_-9wuGK8W9PRPEHy8VOrCOZ-J5EhwYdx8ljEtOBuZTiez5g_vv3npmYn7su'),
(4, 'Angelina Gutkowski', 'wisozk.lola@example.net', '2019-08-28 10:44:04', '$2y$10$HR/YKEoek8iLyAjzo910iuq9BlmqUFF/75/7PqqQ6o4aU4mH9Ke6C', NULL, '2020-11-28', 1, 'admin', 'l5rLqnprb7', 'msoSI4VICJEXdublRaaqR8XP5', NULL, NULL, NULL),
(13, 'Mr. Randy Lind', 'vhowe@example.net', '2019-08-28 10:44:04', '$2y$10$VIsqlCrYOsr1nN5yAPSLweJYiokjeWZVjjiL.GrE37XihMq0fXb9S', NULL, '2020-11-28', 0, 'admin', 'K4LNFD7c2X', 'SxVLrReAQH4VGhVrpqdz4hulW', NULL, NULL, NULL),
(19, 'Gonzalo Krajcik DDS', 'hohara@example.net', '2019-08-28 10:44:05', '$2y$10$yFbQv44CDHCWzxWGC3/A6OgeIz5J1PJ7c92Yo42fh2k2H6K2eUj2O', NULL, '2020-11-28', 0, 'admin', 'VkAZTTVQoJ', 'ZSTRYt3DIWA5bj6A91xfFi2zz', NULL, NULL, NULL),
(22, 'Prof. Alexandra Russel', 'pearl23@example.org', '2019-08-28 10:44:05', '$2y$10$P/P7Q1xDEfbZBgrM6oRCtOaRqVEjxu7OqukDb8PniSQQgVqLefEKi', NULL, '2020-11-28', 1, 'admin', 'HNWCEXHHw5', 'VdpKQ6llWrQN1eY2P60xYB1ma', NULL, NULL, NULL),
(27, 'Mireya Swaniawski', 'dannie37@example.net', '2019-08-28 10:44:05', '$2y$10$WIVJha8tT6EYtcza6C/IyOQF4ekGPnyCCPPPd/515jd5pQ3fNFiAW', NULL, '2020-11-28', 1, 'admin', 'kXGbAybXeT', '3e8rE06tLSzaBom87xl7yilPd', NULL, NULL, NULL),
(28, 'Candice Kling', 'lehner.tad@example.org', '2019-08-28 10:44:05', '$2y$10$HwwixZdRSsEPHhN7.mava.A6jU/nlOo4xeSvArEpBWk6Zp8E3k5Qm', NULL, '2020-11-28', 0, 'admin', 'vEQlLIWvQU', 'R6OQFkxM0uyCIKUQquppyqGRF', NULL, NULL, NULL),
(31, 'Patsy Weissnat', 'ondricka.vance@example.com', '2019-08-28 10:44:05', '$2y$10$6D8VRqOuNIUq.dAMSol/8u285c4ZY/cSXjqX2VnERhtVtdeEiU.6e', NULL, '2020-11-28', 1, 'admin', 'QAv7aMh330', 'ThwB6E6Jo8SMAONAJHknwoDTy', NULL, NULL, NULL),
(34, 'Rubie Jacobs', 'mohr.isobel@example.com', '2019-08-28 10:44:06', '$2y$10$sSIvVLIzISO0J9o5tc/U9OOBlxhMOZZApNNJjiTwT0ST8.4ZqvT5u', NULL, '2020-11-28', 1, 'admin', '0SzuunejW0', '86ol7lmCQtS6I7cOq9QUQ3LJM', NULL, NULL, NULL),
(41, 'Rico Farrell', 'austen.dietrich@example.com', '2019-08-28 10:44:06', '$2y$10$fRl/8lQvUwhapigMnvOTceJg9eyglvtRjqwlsbZr2etzxVMH/x4yW', NULL, '2020-11-28', 1, 'admin', 'OiYc8HWCIo', '5KiEeA2HOgbubWtk2AN0F48cA', NULL, NULL, NULL),
(48, 'Milo Mante', 'terry.miller@example.com', '2019-08-28 10:44:06', '$2y$10$OJ9lDdwRnQ4U3GCi9mg24uEmBeAfB7pu0r3k3KKnkFBUptEOrTKLa', NULL, '2020-11-28', 0, 'admin', 'RNUAoDY1RE', 'vpfZ5WgoZp1giHlCPCLhwtmhw', NULL, NULL, NULL),
(53, 'Yolanda Bayer', 'unicolas@example.org', '2019-08-28 10:44:07', '$2y$10$2DiHtE61Eb3Y5R7tnVbEyeHXu8aXCXd6SlCy.i.BbILVCG.GvDCpi', NULL, '2020-11-28', 1, 'admin', 'J5UPg5IVTh', 'cpCxfAFw4wXZi7sfvR0qs3HYQ', NULL, NULL, NULL),
(54, 'Milos Paunovic', 'milos.paunovic@itcentar.rs', '2019-08-28 10:44:07', '$2y$10$FgO78jZL7HJTb9POI9wv0uplpzrQFb0m3jdlca0lxF1Pcg.eGqqFG', '00381641486214', '2020-11-28', 0, 'admin', 'FYQPrRHRZp', 'go3viDsn0litOj142N21e8GCZ', NULL, '2020-01-09 15:00:52', NULL),
(69, 'Petko Petkovic', 'petko@mail.com', NULL, '$2y$10$3f/Sq83p3oMO7j8taNeyLuPwD86b0CZf.mb2g.CY7yu5YA7thr5.S', NULL, '2019-12-11', 1, 'admin', NULL, NULL, NULL, '2019-12-30 11:26:22', NULL),
(71, 'Janko Jankovic', 'janko@mail.com', NULL, '$2y$10$peZd18rZEV7ctkhLXFy3S.vuaGobRF3wZhRAsoZ4kxrgCvCu10uYe', NULL, '2019-12-26', 0, 'admin', NULL, NULL, '2019-12-24 13:18:36', '2019-12-30 11:35:37', NULL),
(72, 'Milos Paunovic', 'milos@mail.com', NULL, '$2y$10$U7JAh70PNHXDYcLp.L3zh.5pL8IeCxhRJIUeb1WU4lWNI4GrnYNAC', NULL, '2019-12-10', 1, 'admin', NULL, NULL, '2019-12-24 13:45:47', '2019-12-24 13:45:47', NULL),
(73, 'Vladislav Cosic', 'vlada@mail.com', NULL, '$2y$10$5cQoDC9PAB7pcPK.7zxpye6Dq6qMYA1PReJ.esCLsAmClphzjpTLW', '0644146866', '2019-12-18', 1, 'admin', NULL, NULL, '2019-12-24 13:47:40', '2019-12-24 13:47:40', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apiars`
--
ALTER TABLE `apiars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `apiars_user_id_foreign` (`user_id`);

--
-- Indexes for table `hives`
--
ALTER TABLE `hives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hives_apiar_id_foreign` (`apiar_id`),
  ADD KEY `hives_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apiars`
--
ALTER TABLE `apiars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `hives`
--
ALTER TABLE `hives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apiars`
--
ALTER TABLE `apiars`
  ADD CONSTRAINT `apiars_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hives`
--
ALTER TABLE `hives`
  ADD CONSTRAINT `hives_apiar_id_foreign` FOREIGN KEY (`apiar_id`) REFERENCES `apiars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hives_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
