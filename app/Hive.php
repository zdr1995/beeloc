<?php

namespace App;

use App\Unit;
use App\LogModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

define('R',6378);
class Hive extends BaseModel
{

    protected $primaryKey='id';
    protected $table= 'hives';
    public $timestamps=false;

    //  15 Minutes report
    protected static $timeTimeout= 60*60*15;
    protected static $resetMovedTimeoutCheck= 60*60*5;
    protected static $resetMovedTimeoutCheckInMinutes= 5;
    protected static $amplitudeLimit= 200;

    //  One day
    protected static $day= 60*60*24;

    protected $fillable=[
        'apiar_id',
        'name',
        'longitude',
        'latitude',
        'notification_sent',
        'moved',
        'reporting',
        'last_reporting',
        'imei',
        'address',
        'battery',
        'unit_id',
        'main'
    ];
    protected $rules=[
        'apiar_id'     => 'exists:apiary',
        'name'          => 'string|required',
        'longitude'     => 'string',
        'latitude'      => 'string|required',
        'reporting'     => 'boolean',
        'last_reporting'=> 'date',
        'imei'          => 'required|string',
        'unit_id'       => 'required|string'
    ];

    public function units()
    {
        return $this->belongsTo('\App\Unit','unit_id','id');
        // return $this->belongsTo('\App\Unit','units.id');
    }
    public function apiars()
    {
        // return $this->hasMany()
        return $this->hasMany('\App\Apiar','id','apiar_id');
    }
    /**
     * User who has this hive
     *
     * @return User | null
     */
    public function user()
    {
        if($this->apiars->count()>0)
            return $this->apiars[0]->users;
        else return null;
    }

    public function isLicenseActive()
    {
        Log::info('Is license active: ',['is active'=>($this->apiars[0]->users->isActiveLicense())]);
        return $this->apiars[0]->users->isActiveLicense();

    }

    public function addAttributes($data=[],$type)
    {
        foreach($data as $attr=>$value) {
                $this->attributes[$type]=$value;
        }
    }
    public function create(Request $r)
    {
        $instance=new Hive($r->only($this->fillable));
        // dd($instance);
        $instance->save();
        $unit=Unit::UpdateOrCreate($r->only('type'));
        $main=0;
        ($r->type=='Central Unit' || $r->type=='CU')?$main=1:$main=0;

        return true;
    }
    //  List of moved units that has moved
    public static function movedHives()
    {
        $rows= static::where('reporting','=',0)->get();
        $data= collect();
        foreach($rows as $row)
        {
            $apiary= Apiar::where('id',$row->apiar_id)->first();
            if(isset($apiary) && $apiary->sleep==1)
            {
                $data[]=$row;
            }
        }
        return $data;
    }
    public static function notifyUsersForMovedHives(array $ids)
    {
        foreach($ids as $id)
        {
            $row= static::find($ids)->first();
            if($row==null)
                continue;
            $user=$row->apiars[0]->users;
            if(isset($user))
            {
                $data=[
                    'message'=> 'Doslo je do pomeranja kosnice '.$row->apiars[0]->ime,
                    'info'=> 'Neki data',
                    'type'=> 'moved',
                    'title'=> 'Obavestenje',
                ];
                $user->sendNotification($data,$user->id);
            }
        }
        return true;
    }
    public static function checkNotificationInApiary($apiary_id)
    {
        $hives= static::where('apiar_id','=',$apiary_id)->get();
        foreach ($hives as $hive)
        {
            $hive->checkNotificationTimeout();
        }
    }
    public function checkNotificationTimeout()
    {
        if((int)($this->last_reporting)+static::$timeTimeout<\time())
            $this->notification=0;
    }

    //  List of all units from app
    public static function allUnits()
    {
        return DB::select(DB::raw("SELECT COUNT(*)as number FROM hives"))[0]->number;
    }
    public function notifyByPusher()
    {
        if(isset($this->apiars[0]) && isset($this->apiars[0]->users))
        {
            $hives=$this->apiars[0]->users->hives(true);
            $all_data=[];
            $all_data['user']=$this->apiars[0]->users->id;
            $data=[];
            foreach($hives as $apiary_hive)
            {
                foreach ($apiary_hive as $hive)
                {
                    // Different than potera unit
                    if($hive->unit_id!=4)
                    {
                        $data[]=[
                            'id'=> $hive->id,
                            'last_reporting'=> $hive->last_reporting,
                            'name'=> $hive->name,
                            'longitude'=> $hive->longitude,
                            'latitude'=> $hive->latitude,
                            'battery'=> $hive->battery,
                            'moved'=> $hive->moved
                        ];
                    }
                }
            }
            $all_data['hives']=$data;
            event(new \App\Events\LocationEvent(($all_data)));
        }
    }
    public function processAddressList(Request $r)
    {
        $addresses= \explode(',',$r['List']);
        // dd($addresses);
        foreach($addresses as $address)
        {
            $result=preg_match('/^[\d][\d][(...)][\d][\d][(...)]/',$address);
            if($result>0)
            {
                $hive_addr= \explode('(',$address);
                $unit_addr= (int)$hive_addr[0];

                $second_piece= \explode(')',$hive_addr[1]);
                $unit_battery=(int) $second_piece[0];

                $hive= static::where('imei',$this->imei)
                                    ->where('address','=',$unit_addr)
                                    ->first();
                if($hive!== null)
                    $hive->updateStatus($unit_battery);
            }
        }
    }
    public function updateStatus($battery)
    {
        $this->last_reporting= date('Y-m-d H:i:s');
        $this->battery= $battery;
        $this->save();
    }
    public function isNotificationsEnabled()
    {
        return ($this->apiars[0]->sleep)?true:false;
    }
    public function insertIntoLog(array $data)
    {
        $log= new LogModel();
        $log->request= \json_encode($data);

        // If not set List this unit is moved
        if(!isset($data['List']))
        {
            $log->type= 'moved';
            // If object exists in db
            //  Request is handled
            if(isset($this->id))
            {
                $log->hive= \json_encode($this);
            }else {

            }
        }else {
            $log->type= 'reporting';
            $log->report_request= \json_encode($data['List']);
            //  Parse units
            $units=[];
            $log->report_unit= \json_encode($this->processUnitsReportList($data['List']));
        }

        try {
            $log->save();
        }catch(\Exception $e) {
            Log::error('Cannot insert into log');
        }
    }

    /**
     * Undocumented function
     *
     * @param string $addresses
     * @return array
     */
    public function processUnitsReportList($address)
    {
        $addresses= \explode(',',$address);
        $data=[];
        foreach($addresses as $address)
        {
            $unit_data=[];
            // dd($addresses);
            // dd(preg_match('/^[\d][\d][(...)][\d][\d][(...)]/',$addresses[1]) || preg_match('/^[\d][\d][(...)][\d][\d][\d][(...)]/',$addresses[1]));
            /*
            |---------------------------------------------------------------
            |   Because data format is 23(100),20(86)
            |---------------------------------------------------------------
            */
            $result=preg_match('/^[\d][\d][(...)][\d][\d][(...)]/',$address) || preg_match('/^[\d][\d][(...)][\d][\d][\d][(...)]/',$address);
            if($result)
            {
                $hive_addr= \explode('(',$address);
                $unit_addr= (int)$hive_addr[0];
                $unit_data['address']= $unit_addr;

                $second_piece= \explode(')',$hive_addr[1]);
                $unit_battery=(int) $second_piece[0];

                $unit_data['battery']= $unit_battery;

                $data[]= $unit_data;
            }
        }
        return $data;
    }
    /**
     * ----------------------------------
     * This method is called by cron job
     * ----------------------------------
     * 
     * Check time between last reporting
     * and if neccessary notify user
     *
     * @return void
     */
    public function checkHiveMovedStatus()
    {
//        (new \App\Jobs\NotificationJob($this))->delay(2);
        $now=Carbon::now();
        $last_report= new Carbon($this->last_reporting);
        if($this->isTimeToReset())
        {
            // greather than 5 minutes
            if($this->moved==1)
            {
                $this->moved= 2;
                $this->reporting=1;
                $this->save();
            }
        }else {
            if($this->moved==1 && $this->user()!==null && $this->notification_sent==0 && $this->isNotificationsEnabled())
            {
                if(isset($this->apiars) && $this->apiars[0]->sleep==1)
                {
                    if($this->reporting==1)
                        $this->user()->sendHiveMovedNotification($this);
                    $this->notification_sent=1;
                }
            }
        }
        $this->save();
    }
    /**
     * Calculate time between now
     * and last reporting
     * with defined property
     * resetMovedTimeoutCheck
     *
     * @return boolean
     */
    public function isTimeToReset()
    {
        $now=Carbon::now();
        $last_report= new Carbon($this->last_reporting);
        return isset($this->last_reporting) && ($now->diffInMinutes($last_report) > static::$resetMovedTimeoutCheckInMinutes);
    }
    public function CalculateDistance(array $data)
    {
        if(
            !isset($data['longitude']) ||
            !isset($data['latitude'])
        )
        return 'failed';

        $longitude= $data['longitude'];
        $latitude= $data['latitude'];

        //  Get longitude and latitude in radians
        $lng= $longitude/(180/\pi());
        $lat= $latitude/(180/\pi());

        $this_lng= $this->longitude/(180/\pi());
        $this_lat= $this->latitude/(180/\pi());

        $dist= $this->point2point_distance($longitude,$latitude,$this->longitude,$this->latitude);
        return $dist;
    }
    public function point2point_distance($lat1, $lon1, $lat2, $lon2, $unit='K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K")
        {
            return ($miles * 1.609344);
        }
        else if ($unit == "N")
        {
        return ($miles * 0.8684);
        }
        else
        {
        return $miles;
        }
    }
    public function changeApiarChangeStatus()
    {
        try {
            $this->apiars[0]->change=1;
            $this->apiars[0]->save();
        }catch(\Exception $e) {
        }
    }

    public function postUpdate(Request $r, $id)
    {
        $apiar= $this->apiars[0];
        foreach ($apiar->hives as $hive)
        {
            if($hive->moved==1)
                return true;
        }
        if($r['moved']==0)
        {
            $apiar->change=0;
            $apiar->save();
        }

        return true;
    }
    /**
    *---------------------------------------------
    * Check daily moving report status
    *---------------------------------------------
    * @return void
    */
    public function checkDailyReport()
    {
        $now= Carbon::now();
        $last_report= Carbon::create($this->last_reporting);
        $difference_in_days= abs($now->diffInDays($last_report));
        if($difference_in_days) 
        {
            DB::transaction(function() {
                DB::table('hives')->where('id','=',$this->id)->update(['reporting'=>'0']);
                DB::table('apiars')->where('id','=',$this->apiar_id)->update(['change'=>'1']);
            });
        }
    }
    /**
     * -------------------------------------------
     *  Process communication data for cu
     * -------------------------------------------
     * @return void
     */
    public function processCuData(Request $r,array $data)
    {
        $this->longitude= $data['longitude'];
        $this->latitude= $data['latitude'];
        $this->last_reporting= date('Y-m-d H:i:s');
        $this->save();

        // if(isset($data['Vamp']) && intval($data['Vamp'])>360 && $hive->isLicenseActive())
        if(isset($data['Vamp']) && intval($data['Vamp'])>static::$amplitudeLimit)
        {
            $this->reporting=1;
            // $this->moved=1;
            if($r->has('P'))
            {
                $this->changeApiarChangeStatus();
                //  If last moving has 1
                if($this->moved==0)
                {
                    if($this->isNotificationsEnabled()) 
                        (new User)->sendHiveMovedNotification($this);
                    $this->moved= 1;
                    $this->reporting=1;
                    $this->notification_sent= 0;
                }else {
                    $this->moved= 1;
                    $this->reporting=1;
                }
            }else {
                $this->moved=0;
            }
            // if($this->isNotificationsEnabled())
            // {
            //     $this->save();
            //     (new User)->sendHiveMovedNotification($this);
            // }
            if(isset($this->apiars[0]->users) && $this->isUserReadyToListen())
            {
                $this->notifyByPusher();
            }
        }
        $this->save();
        $this->insertIntoLog($data);
    }
    /**
     * -----------------------------------------------
     *  Processing communication data for non cu
     * -----------------------------------------------
     * @return void
     */
    public function processHiveData(array $data)
    {
        if($this->unit_id!=4 &&
            isset($data['longitude']) &&
            isset($data['latitude'])
        )
        {
            $this->longitude= $data['longitude'];
            $this->latitude= $data['latitude'];
        }
        $this->last_reporting= date('Y-m-d H:i:s');
        $this->save();
    //                if(isset($data['Vamp']) && intval($data['Vamp'])>360 && $hive->isLicenseActive())
        if(isset($data['Vamp']) && intval($data['Vamp'])>static::$amplitudeLimit)
        {
            $this->reporting=1;
            //  If has moving parameter
            if($r->has('P'))
            {
                $this->changeApiarChangeStatus();
                $this->notification_sent=0;
                //  If last moving has 1
                if($this->moved==0)
                {
                    (new User)->sendHiveMovedNotification($hive);
                    $this->moved= 1;
                    $this->reporting=1;
                }else {
                    $this->moved= 1;
                    $this->reporting=1;
                }
            }else {
                $this->moved=0;
            }
            if($this->isNotificationsEnabled())
            {
                $this->save();
                (new User)->sendHiveMovedNotification($hive);
            }
            // dd($this->apiars[0]->users);
            if(isset($this->apiars[0]->users) && $this->isUserReadyToListen())
            {
                $this->notifyByPusher();
            }
        }
        $this->save();
        $this->insertIntoLog($data);
    }

    public function isUserReadyToListen()
    {
        return $this->apiars[0]->users->ready_to_listen==1;
    }

}
