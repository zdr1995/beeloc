<?php

namespace App;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    public function getLoggedIn()
    {
        try{
            $user=(Auth::user())?\Auth::user():\JWTAuth::user();
            return $user;
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return false;
        }
    }
    public static function userApiars()
    {

        $user=(Auth::user())?\Auth::user():\JWTAuth::user();
        \Log::info('user Jwt',['user'=>\JWTAuth::User()]);
        \Log::info("user id",['id'=>\Auth::User()]);

        if($user)
        {
            $apiars=$user->apiars;
            foreach ($apiars as $apiar)
            {
                $apiar->battery=$apiar->mainCU()['battery'];
                $apiar->reporting=$apiar->mainCU()['reporting'];
            }
            return response()->json($apiars);
        }else return response()->json(['message'=>'invalid creditials'],404);
    }

    public static function loadHives($apiarId)
    {
        // dd($apiarId);
        $apiar= Apiar::where('id',$apiarId)->first();
        if(!isset($apiar))
            return response()->json(['message'=>'not found'],404);

        Hive::checkNotificationInApiary($apiarId);
        $hives=Hive::where('apiar_id',$apiarId)->orderBy('unit_id')->get();
        foreach($hives as $key=>$hive)
        {
            $hive->type=\App\Unit::where('id',$hive->unit_id)->first()->name;
            $hive->cu_address= $apiar->mainCU()->address;
        }
        return response()->json($hives);
    }

    public function del()
    {
        if($this->delete())
            return response()->json(['message'=>'Successifully deleted'],204);
        return response()->json(['message','not-found'],404);
    }

    public function customUpdate(Request $r, $id)
    {
        return true;
    }
    public function postUpdate(Request $r, $id)
    {
        return true;
    }
}
