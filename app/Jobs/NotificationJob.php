<?php

namespace App\Jobs;

use App\Hive;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $hive;
    public $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Hive $hive)
    {
        $this->hive= $hive;
        $this->user= $this->hive->user();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(isset($this->user))
            $this->user->sendHiveMovedNotification($this->hive);
//        dd("to jeto");
        Log::info('Worker radii');
        // $this->hive->address= (int)$this->hive->address + 10;
        // $this->hive->save();
    }
}
