<?php


    namespace App\Http\Middleware;
    use Illuminate\Support\Facades\Auth;

    use Closure;

    class LoggedIn
    {
        public function handle($request, Closure $next)
        {
            $user= Auth::check();
            if(!$user)
                return redirect('/login');
            else return $next($request);
        }
    }

?>