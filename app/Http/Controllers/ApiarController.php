<?php

namespace App\Http\Controllers;

use App\Apiar;
use App\BaseModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class ApiarController extends Controller
{

    public function apiarAddView()
    {
        $user= $this->getLoggedIn();
        if($user->isAdmin())
        {
            return view('templates/apiar/apiar-add');
        }else {
            redirect('login');
        }
    }
    public function changeSleepStatus(Request $r, $user_id, $id)
    {
        // dd($_SERVER);
        $row= Apiar::where('id','=',$id)->first();
        $loggedIn= $this->getLoggedIn();
        if(isset($row) && $loggedIn->role!='user')
        {
            $row->sleep=($row->sleep)?0:1;
            foreach ($row->hives as $hive)
            {
                $hive->reporting= 0;
                $hive->moved=0;
                $hive->save();
            }
            $row->save();
            return redirect('/user/list/'.$user_id.'/true');

        }else return Route::post('/user/list/'.$user_id.'/true',['user'=>$loggedIn]);
    }
    public function loadHives($apiarId)
    {
        $data['apiary']= Apiar::where('id','=',$apiarId);
        $data['hives']= BaseModel::loadHives($apiarId);
        return View('templates/apiar/single',['apiar'=>$data]);
    }
    public function searchApiars(Request $r)
    {
        $apiars= \App\Apiar::select('apiars.*');
        if(isset($r['search']))
        {
            if(isset($r['status']) && $r['status']!=='Status')
            {
                Session::flash('status', $r['status']);
                if($r['status']=='active')
                    $apiars= $apiars->join('users','apiars.user_id','=','users.id')->where('users.expire','>', \time());
                else
                    $apiars= $apiars->join('users','apiars.user_id','=','users.id')->where('users.expire','<', \time());
            }
            $apiars= $apiars->where('apiars.name','like','%'.$r['search'].'%');
            \session()->flash('search', $r['search']);
        }else {
            if(isset($r['status']))
            {
                Session::flash('status', $r['status']);
                if($r['status']=='active')
                    $apiars= $apiars->load('hives')->load('users')->where('users.expire','>', \time());
                else
                    $apiars= $apiars->load('hives')->load('users')->where('users.expire','<', \time());
            }
        }
        return View('templates/apiar-list',['apiars'=>$apiars->get()]);

    }
    public function insertNew(Request $r)
    {
        $user= $this->getLoggedIn();
        if($user->role!=='admin')
            return redirect('login');

        $apiar= new Apiar();

        if(!$apiar->validate($r->all()))
            return redirect('login');

        $apiar->name= $r['name'];
        $apiar->sleep=0;
        $apiar->change=0;
        $apiar->user_id= $r['user_id'];

        if(!$apiar->save())
        {
            Session::flash('color','red');
            Session::flash('message','Nije moguce dodati jedinicu');
            return redirect('dashboard');
        }

        if($apiar->insertMainCU($r))
        {
            Session::flash('color','green');
            Session::flash('message','Uspesno ste dodali Pcelinjak');
            return redirect('dashboard');
        }
        else return redirect('login');

    }
    public function delete(Request $r)
    {
        if(!isset($r['apiar_id']) || $this->getLoggedIn()->role!=='admin')
            return back()->withInput();
        $apiary= Apiar::where('id','=',$r['apiar_id'])->first();
        if( $apiary=='null' || $apiary==null)
            return redirect('apiars/list/true');

        try {
            $apiary->delete();
        }catch(\Exception $e) {
            return redirect('apiars/list/true');
        }
        return redirect('apiars/list/true');

    }
    public function loadApiarEditView(Request $r, $id)
    {
        $apiar= Apiar::where('id','=',$id)->first();
        if($apiar==null)
            return redirect('apiar/list/true')->withInput();
        else return view('templates/apiar/edit', ['apiar'=>$apiar]);
    }
    public function editApiar(Request $r, $id)
    {
        $apiar= Apiar::where('id','=',$id)->first();
        if($apiar!==null)
        {
            if($apiar->edit($r))
                return redirect('apiars/list/true')->withInput();
            else {
                \session()->flash('message','Doslo je do greske, pokusajte ponovo');
                return redirect('apiars/list/true')->withInput();
            }
        }else {
            Session::flash('message','Doslo je do greske, pokusajte ponovo');
            return redirect('apiars/list/true')->withInput();
        }
    }
    public function deleteMany(Request $r)
    {
        if(!$r->has('apiar_ids'))
        {
            Session::flash('color','red');
            Session::flash('message','Ne mozete obrisati pcelinjak');
            return redirect('apiars/list/true');
        }

        $ids= \json_decode($r['apiar_ids']);
        \DB::table('apiars')->whereIn('id',$ids)->delete();
        Session::flash('color','green');
        Session::flash('message','Uspesno ste obrisali pcelinjake');
        return redirect('apiars/list/true');
    }
}
