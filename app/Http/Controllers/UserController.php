<?php

namespace App\Http\Controllers;

use App\create_user;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getRelationship()
    {
        $user=User::all();
        $data=[];
        for($x=0;$x<count($user);$x++)
        {
            $main=[];
            $userData=\App\User::find($user[$x]['id']);
            $main[]=($userData);
            $relation=$userData->apiar;
            $main[]=($relation);
            $data[]=$main;
        }

        // dd(\App\User::find(5)->apiar);
        // dd($user->apiar);
        // dd($user->apiar->attach('apiar'));
        return response()->json($data);
    }

    public function loginUser(Request $r)
    {
        $user=new User();
        return $user->login($r);
    }
    public function addUserView(Request $r)
    {
        return View('templates/user_add');
    }
    public function addUser(Request $r)
    {
        $user= new User();
        $user->fill($r->all());
        $user->status= ((time()-strtotime($r['expire']))>0)?1:0;
        $user->password= \Hash::make($user->password);
        if(!$user->save()) {
            Session::flash('message','<b>Check out all fields</b>');
            return view('templates/user_add',$user);
        }else {
            return redirect('users/list');
        }
    }

    public function loginWeb(Request $r)
    {
        $user=new User();
        if($user || $user->isAdmin())
        {
            $data=$user->loginWeb($r);
            if($data)
                return redirect('dashboard');
            else redirect('login');
        }else return redirect('login');
    }
    public function logout(Request $r)
    {
        (\Auth::user())?$user=\Auth::user():$user=\JWTAuth::parseToken()->authenticate();
        if(! $user) {
            return response()->json(['message'=>'Error'],404);
        }else{
            try {
                $token= JWTAuth::parseToken()->getToken();
//                dd($token);
                $user->fcm_token= null;
                $user->save();
                \Auth::logout() && \JWTAuth::invalidate($token);
                return response()->json(['message'=>'Success'],200);
            }catch(JWTException $e) {
                return response()->json(['message'=>'not found'],404);
            }
        }
    }
    public function storeFCMToken(Request $r)
    {
//        \Log::info("Request za token",(array)($r->all()));
//        if($r->has('fcm_token'))
//            \Log::info("token",(array)(['token'=>$r->fcm_token]));
        //return response()->json(['that\'s is'],200);
        $user=JWTAuth::parseToken()->authenticate();
//        \Log::info("User je :",(array)($user));
        if($user && $user->updateFCM($r))
            return response()->json('successifully',200);
        return response()->json('successifully',200);
//        return response()->json(['message'=>'not found'],404);
    }
    public function notify(Request $r,$id,$type='move')
    {
        $data=[
            'data'=>[
                'message'=>$r['message'],
                'info'=> $r['info']
            ],
            'title'=> $r['title']
        ];
        $user=new User;
        if($user->sendNotification($data,$id))
            return response()->json('successifully',200);
        return response()->json('not found',404);
    }
    public function delete(\Illuminate\Http\Request $r)
    {
        $user= \Auth::user();
        if(isset($user) && $user->role=='admin' && isset($r['id']))
        {
            $user= \App\User::where('id','=',$r['id'])->first();
            if(isset($user) && $user->delete())
                return redirect($_SERVER['HTTP_REFERER']);
        }
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function changeListenStatus(Request $r)
    {
//        dd('Stize: ',(array)$r->all());
//        dd($r->has('isActive'));
        \Log::info('Stize: ',(array)$r->all());
//        if(!$r->has('isActive'))
//            return response()->json(['message'=>'not found'],404);
//
        if( !(new User)->patchListenStatus($r) )
            return response()->json(['message'=>'not found'],404);
        return json_encode('Pusher radi');
    }
    public function serveEditView(Request $r, $id)
    {
        $loggedIn= $this->getLoggedIn();
        if($loggedIn->role!=='admin')
            return redirect('login');

        $user= User::where('id','=',$id)->first();
        if(!isset($user))
            return redirect('login');
        return View('templates/user/edit',['user'=>$user,'loggedIn'=>$loggedIn]);
    }
    public function edit(Request $r, $id)
    {
        $loggedIn= $this->getLoggedIn();
        if($loggedIn->role!=='admin')
            return redirect('login');

        $user= User::where('id','=',$id)->first();
        if(!isset($user))
            return redirect('login');

        $user->fill($r->except('password'));
        $user->save();
        return redirect($_SERVER['HTTP_REFERER']);
    }
    public function searchUsers(Request $r)
    {
        // dd($r->all());
        // dd($r['search']);
        $users= User::select();
        if(isset($r['search']))
        {
            $users= \DB::table('users')->where(function($query) use($r) {
                if(isset($r['status']) && $r['status']!=='Status')
                {
                    $status=($r['status']=='active')?1:0;
                    $query->where('status','=',$status);
                    $query->where(function($query2)use($r) {
                        $query2->where('name','like','%'.$r['search'].'%')
                            ->orWhere('email','like','%'.$r['search'].'%');
                    });
                }else {
                    $query->where('name','like','%'.$r['search'].'%')
                        ->orWhere('email','like','%'.$r['search'].'%');
                }

            });
            $users=$users->get();
            $search=$r['search'];
            if($r['reset']!=='reset')
                \session()->flash('search', $r['search']);
            // dd($users->get());
            return View($r['view'],compact('users','search'));

        }else {
            $users= $users->get();
            return View($r['view'],compact('users'));
        }
    }
    public function logoutWeb(Request $r)
    {
        try{
            $user= \Auth::user();
            \Auth::logout();
            return redirect('/login');
        }catch(\OAuthException $e) {
            return redirect('/login');
        }
    }
    public function deleteMany(Request $r)
    {
        // dd(\json_decode($r['user_ids']));
        if(!$r->has('user_ids'))
        {
            Session::flash('message','Ne mozete obrisati korisnike');
            return redirect('users/list');
        }
        $user_ids= \json_decode($r['user_ids']);
        // dd($user_ids);
        foreach($user_ids as $id)
        {
            $query="
                DELETE 
                FROM users
                WHERE id=$id
            ";
            \DB::select(\DB::raw($query,['id'=>$id]));
        }
        Session::flash('message','Uspesno ste obrisali korisnike');
        return redirect('users/list');
    }



}



// $rel=$user->apiar;
// $relation=[];
// $rel->map(function($rel){
//     $relation[]=$rel;
// });
