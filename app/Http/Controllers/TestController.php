<?php

namespace App\Http\Controllers;

use App\Hive;
use App\User;
use Illuminate\Http\Request;
use Benwilkins\FCM\FcmMessage;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notification;
use Symfony\Component\Console\Output\BufferedOutput;

class TestController extends BaseController
{
    public function testHives(Request $r)
    {
        $hive=Hive::where('id','=',1)->first();
        dd($hive->isLicenseActive());
    }

    public function notifyLicense(Request $r)
    {
        // $id= $r['id'];
        $data=[
            'title'=>$r['title'],
            'message'=>$r['message'],
            'click_action'=> 'OPEN_LICENCE',
            'extra'=> 'open_licence',
            'type'=> 'licence'
        ];
        // $token= \App\User::where('id','=',1)->first()->fcm_token;
        // dd($token);
        // $message= new \Fcm();


        // $fcm= new FcmMessage();

        $user=new User;
        if($user->sendNotification($data,$r['id']))
            return response()->json('successifully',200);
        return response()->json('not found',404);
    }
    public function jwtGenerate(Request $r)
    {
        try{
            \Artisan::call('jwt:generate');
        }catch(\Exception $e) {
            \Log::info('Dosao ovdje',(array)($e));
        }

    }
    public function pusherView()
    {
        return view('Test/pusher');
    }
    public function testRegex(Request $r)
    {
        $hive= Hive::where('id','=',10)->first();
        dd($hive->isNotificationsEnabled());
    }
    public function communicateTest(Request $r)
    {
        Log::info('Request',(array)$r->all());
        if(isset($r['I']))
        {
            $imei=$r['I'];
            $long_lat=$r['L'];
            $long_lat_arr= \explode(',',$long_lat);
            $longitude= $long_lat_arr[1];
            $latitude= $long_lat_arr[0];
            $amplitude= $r['Vamp'];
            $addr= $r['Addr'];
            $data=[
                'longitude'=> $longitude,
                'latitude'=> $latitude,
                'IMEI'=> $imei,
                'Vamp'=> $amplitude,
                'Addr'=> $addr,
                'BatCU'=> $r['BatCU'],
                'List'=> $r['List']
            ];

            $hive= Hive::where('imei','=',$data['IMEI'])
                ->where('address','=',$data['Addr'])
                ->first();
            Log::info('Hive: ',(array)$hive);
            if($r->has('List'))
            {
                $hive= \App\Hive::where('imei','=',$data['IMEI'])
                                    ->where('unit_id','=',1)
                                    ->first();
                if($hive!==null) {
                    $hive->processAddressList($r);
                    $hive->insertIntoLog($data);
                }
            }else {
//            if($hive!==null && $hive->isLicenseActive())

                $cu= Hive::where('imei','=',$data['IMEI'])
                                ->where('unit_id','=',1)
                                ->where('address','=',$data['Addr'])
                                ->orWhere('address','=','null')
                                ->first();
                if($cu!==null)
                {
                    $cu->longitude= $data['longitude'];
                    $cu->latitude= $data['latitude'];
                    $cu->last_reporting= date('Y-m-d H:i:s');

                    //                if(isset($data['Vamp']) && intval($data['Vamp'])>360 && $hive->isLicenseActive())
                        if(isset($data['Vamp']) && intval($data['Vamp'])>360)
                        {
                            $cu->reporting=1;
                            $cu->moved=1;
                    //                    dd($hive->apiars[0]->users);
                        }
                    $cu->save();
                    $cu->insertIntoLog($data);
                }else if($hive!==null) {
                        if($hive->unit_id==3)
                        {
                            $hive->longitude= $data['longitude'];
                            $hive->latitude= $data['latitude'];
                        }
                        $hive->last_reporting= date('Y-m-d H:i:s');
                    //                if(isset($data['Vamp']) && intval($data['Vamp'])>360 && $hive->isLicenseActive())
                        if(isset($data['Vamp']) && intval($data['Vamp'])>360)
                        {
                            $hive->reporting=1;
                            $hive->moved=1;
                    //                    dd($hive->apiars[0]->users);
                            if(isset($hive->apiars[0]->users) && $hive->apiars[0]->users->ready_for_listen==1)
                            {
                                //$hive->notifyByPusher();
                            }
                        }
                        $hive->save();
                        $hive->insertIntoLog($data);
                }
            }
            Log::info("Podaci",$data);
            http_response_code(200);
            return response()->json(['message'=>'success']);
        //            \Log::error("kosnica:I ",['I'=>$r['I']]);
        }else {
            http_response_code(404);
            return response()->json(['message'=>'not found'], 404);
        }
        http_response_code(404);
        return response()->json(['message'=>'not found'], 404);
    }

    public function createJob(Request $r,$id)
    {
        if($id!==null)
        {
            $hive= \App\Hive::where('apiar_id','=',$id)->first();
            if($hive==null)
                $hive= \App\Hive::where('apiar_id','=',2)->first();

        }else {
            $hive= \App\Hive::where('apiar_id','=',2)->first();
        }
        (new \App\User)->sendHiveMovedNotification($hive);
        return response()->json(['message'=>'to je to']);
//        $buffer= new BufferedOutput();
//        $data=[];
////        exec('cd /home/beelocrs/public_html/api/app/ && ls -la',$data);
//        exec('crontab -l',$data);
////        exec('/usr/local/bin/php -v',$data);
////        exec('crontab -e',$data);
//        dd($data);
//        \Artisan::call('list',$data,$buffer);
//        dd($buffer->fetch());
//        return response()->json(['message'=>$buffer->fetch()]);
        // // \Artisan::call('queue:table');
        // $hive=\App\Hive::where('id','=',10)->first();
        // $job= (new \App\Jobs\NotificationJob($hive));

        // // $job= (new \App\Jobs\NotificationJob($hive))->delay(\Carbon\Carbon::now()->addSeconds(100));
        // $this->dispatch($job)->everyMinute();
        // // (new \App\Jobs\NotificationJob($hive))->delay(\Carbon\Carbon::now()->addSeconds(100));
        // return response()->json(['message'=>'Event dispatched']);
    }
    public function phpinfo()
    {
        dd(phpinfo());
    }

}
