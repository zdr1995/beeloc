<?php

namespace App\Http\Controllers;

use App\Hive;
use App\Apiar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HiveController extends Controller
{
    public function testDistance(Request $r)
    {
        if(!$r->has('imei'))
            return response()->json(['message'=>'not found']);

        $hive= Hive::where('imei','=',$r['imei'])->first();
        if(isset($hive))
        {
            $data= [
                'longitude'=>$r['longitude'],
                'latitude'=> $r['latitude']
            ];

            $distance= $hive->CalculateDistance($data);
            return response()->json(['distanca'=>$distance]);
        }
    }
    public function loadByApiaryId(Request $r, $id)
    {
        $hives= Hive::where('apiar_id','=',$id)->orderBy('unit_id')->get();
        \session()->flash('search','hidden');
        if($hives==null)
            return redirect('apiars/list/true');

        // $hives= \DB::select(\DB::raw('
        // 	SELECT hives.unit_id
        // 	FROM hives
        // 	JOIN units
        // 		ON hives.unit_id= units.id
        // 	ORDER BY hives.unit_id
        // '));
        // dd($apiar->hives[0]->units()->where('id','=',1)->get());
        // dd($hives);
        $data['hives']= $hives;
        $data['apiar']= \App\Apiar::where('id','=',$id)->first();
        return View('templates/hive/list',['data'=>$data]);
    }
    public function delete(Request $r)
    {
        if(!$r->has('hive_id'))
            return redirect($_SERVER['HTTP_REFERER']);

        $hive= \App\Hive::where('id','=',$r['hive_id'])->first();
        if($hive==null)
            return redirect($_SERVER['HTTP_REFERER']);

        $hive->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }
    public function loadHiveAddView(Request $r, $apiar_id)
    {
        $apiar= Apiar::where('id','=',$apiar_id)->first();
        return view('templates/hive/add',['apiar' => $apiar]);
    }
    public function loadHiveAdd(Request $r)
    {
        return view('templates/hive/add');
    }
    public function insert(Request $r)
    {
        // dd($r->all());
        $hive= new Hive();
        $data= $r->all();
        $data['main']=0;
        $data['last_reporting']= \date('Y-m-d');
        $hive->fill($data);
        if(!isset($r['apiar_id']))
            return redirect('login');
        if(!$hive->save())
        {
            \session()->flash('message', 'Nije moguce dodati jedinicu');
            \session()->flash('color','red');
            return redirect('/apiar/'.$r['apiar_id'].'/hives');
        }else {
            \session()->flash('message','uspesno dodana jedinica');
            \session()->flash('color','green');
            return redirect('/apiar/'.$r['apiar_id'].'/hives');
        }
    }
    public function loadEditView(Request $r, $id)
    {
        if(!isset($id))
            return redirect($_SERVER['HTTP_REFERER']);
        $hive= Hive::where('id','=',$id)->first();
        if($hive==null)
            return redirect($_SERVER['HTTP_REFERER']);
        return view('templates/hive/edit',['hive'=> $hive]);
    }
    public function editHive(\Illuminate\Http\Request $r, $id)
    {
        if(!isset($id))
            return redirect($_SERVER['HTTP_REFERER']);
        
        $hive= \App\Hive::where('id','=',$id)->first();
        
        if(!isset($hive))
            return redirect($_SERVER['HTTP_REFERER']);

        $hive->fill($r->all());
        if(!$hive->save());
            // return redirect()->back()->withInput();
        return redirect('apiar/'.$hive->apiar_id.'/hives');
    }
    public function searchHive(Request $r)
    {
        // $hives= Hive::with('apiars')->where('apiar_id','=',$r['apiar_id']);

        $query= DB::table('hives')->select(['hives.*','units.name as unit_name']);
        if(isset($r['status']) && $r['status']!=='Status')
        {
            \session()->flash('active',$r['status']);
            if($r['status']=='active') {
                $query= $query->where('users.status','=',1);
            }
            if($r['status']=='inactive'){
                $query= $query->where('users.status','=',0);
            }
        }
        if($r->has('search')) {
            $search= '%'.strtolower($r['search']).'%';    
            \session()->flash('search',$r['search']);
            $query= $query->where("hives.name",'like', $search);
        }
        $query->join('units','units.id','hives.unit_id');
        $query->join('apiars','hives.apiar_id','apiars.id');
        $query->join('users','apiars.user_id','users.id');

        $query->where('hives.apiar_id','=',$r['apiar_id']);
        $data=[
            'apiar'=> Apiar::where('id','=',$r['apiar_id'])->first(),
            'hives'=> $query->get()
        ];
        return \view('templates/hive/list',['data'=> $data]);
    }
    public function deleteMany(Request $r)
    {
        $uri=$_SERVER['HTTP_REFERER'];
        if(!$r->has('hive_ids'))
        {
            Session::flash('color','red');
            Session::flash('message','Neuspesno brisanje');
            return redirect($uri);
        }

        $ids= json_decode($r['hive_ids']);
        if($ids!==null)
        {
            \DB::table('hives')->whereIn('id',$ids)->delete();
            Session::flash('color','green');
            Session::flash('message','Uspesno ste obrisali');
            return redirect($uri);
        }

    }
}
