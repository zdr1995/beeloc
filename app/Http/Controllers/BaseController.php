<?php

namespace App\Http\Controllers;

// use App\BaseModel;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    private static $channel= 'beeloclocation';
    private static $event= 'location';

    public function getLoggedIn()
    {
        try{
            $user=(Auth::user())?Auth::user():JWTAuth::user();
            return $user;
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return false;
        }
    }

    public function loadApiars(Request $r)
    {
        \Log::info('Request za apiar:',['headers'=>$r->headers->all()]);
        return \App\BaseModel::userApiars();
    }

    public function loadHive($apiarId)
    {
        return \App\BaseModel::loadHives($apiarId);
    }

    public function loadMainCu()
    {
        $user=(\Auth::user())?\Auth::user():JWTAuth::parseToken()->authenticate();
        \Log::info('User u loadMain:',(array)($user));
        \Log::info('User in mainCU',(array)$user);
        if($user) {
            return $user->loadMain();
        }else return 'Not authorized';
    }
    ///////////////////////
    //  Web views routes //
    ///////////////////////
    public function listUsers()
    {
        $users=User::all();
        return \View('templates/users-list',compact('users'));
    }
    //  Get Dashboard view
    public function dashboard()
    {
        $user=$this->getLoggedIn();
        if(!isset($user))
            return redirect('login');
        $data=$user->load('apiars')->withRelations();
        $data->status=$user->status();
        return view('dashboard',['user'=>$data]);
    }
    public function listUserData(Request $r,$id,$getView=false)
    {
        $loggedIn= $this->getLoggedIn();
        if(!isset($loggedIn) || $loggedIn->role!='admin')
            return redirect('login');
        $user=\App\User::findOrFail($id);
        $data=$user->load('apiars')->withRelations();
        $query="
            SELECT count(hives.id) as number
            FROM hives,apiars,users
            WHERE hives.apiar_id=apiars.id
            AND apiars.user_id=users.id
            AND users.id=$user->id";
        $data->units_count= \DB::select(\DB::raw($query))[0]->number;

        $query="
            SELECT count(hives.id) as number
            FROM hives,apiars,users
            WHERE hives.apiar_id=apiars.id
            AND apiars.user_id=users.id
            AND hives.unit_id=1
            AND users.id=$user->id;
        ";
        $data->cu_count= \DB::select(\DB::raw($query))[0]->number;

        $query="
            SELECT count(hives.id) as number
            FROM hives,apiars,users
            WHERE hives.apiar_id=apiars.id
            AND apiars.user_id=users.id
            AND hives.unit_id=2
            AND users.id=$user->id;
        ";
        $data->st_count= \DB::select(\DB::raw($query))[0]->number;

        $query="
            SELECT count(hives.id) as number
            FROM hives,apiars,users
            WHERE hives.apiar_id=apiars.id
            AND apiars.user_id=users.id
            AND hives.unit_id=3
            AND users.id=$user->id;
        ";
        $data->stg_count= \DB::select(\DB::raw($query))[0]->number;

        $query="
            SELECT count(hives.id) as number
            FROM hives,apiars,users
            WHERE hives.apiar_id=apiars.id
            AND apiars.user_id=users.id
            AND hives.unit_id=4
            AND users.id=$user->id;
        ";
        $data->pt_count= \DB::select(\DB::raw($query))[0]->number;


        if(isset($r['apiar_id']))
        {
            $apiar= \App\Apiar::where('id','=',$r['apiar_id'])->first();
            if($apiar!==null)
            {
                $data->selected_apiar= $apiar;
            }
        }
        if(!isset($data))
            return redirect('users/list');
        $data->status=$user->status();

        //  Lista svih pcelinjaka za korisnika
        // dd($data);
        if($getView == 'true')
            return view('templates/user-details',['user'=>$data]);
        else return response()->json($data,200);
    }

    public function listApiars(Request $r, $getView=false)
    {
        $apiars=\App\Apiar::orderBy('id','desc')->with('users')->with('hives');
        $ap=response()->json($apiars->get(),200);
        if($getView == 'false')
            return \response()->json($apiars->get(),200);
        return View('templates/apiar-list',['apiars'=>$apiars->get()]);
    }
    public function fireEvent()
    {
//        $pusher=new \Pusher\Pusher(
//            env('PUSHER_APP_ID','933889'),
//            env('PUSHER_APP_KEY','acc339cb29643c3c0759'),
//            env('PUSHER_APP_SECRET'),
//            [
//                'cluster'=>'eu',
//                'useTLS'=>false
//            ]
//        );
        $data=[
            'message'=>'Pusher app fired'
        ];
        for($i=0;$i<100;$i++) {
            event(new \App\Events\LocationEvent('Message fired'));
        }

        return \Response()->json(['message'=>'event fired']);
    }
}
