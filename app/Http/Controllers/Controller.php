<?php

namespace App\Http\Controllers;

use App\Hive;
use App\BaseModel;
use DateTime;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Undocumented function
     *
     * @return \App\User
     */
    public function getLoggedIn()
    {
        try{
            $user=(\Auth::user())?\Auth::user():\JWTAuth::user();
            return $user;
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return false;
        }
    }
    public function makeModel($className)
    {
        return '\App\\'.ucfirst($className);
    }

    public function create(Request $r,$resource)
    {
        $model=$this->makeModel($resource);

        $instance=new $model;
        if( $instance->create($r) )
        {
            $message="$resource succefully created.";
            return response()->json(['message'=>$message],200);
        }else {
            return response()->json(['message'=>'Something wen\'t wrong'],404);
        }

    }

    public function loadAll(Request $r,$resource)
    {
        $model=$this->makeModel($resource);
        $instance= new $model;
        if($instance instanceof \App\User)
            return $instance->loadAll();
        return $instance->all();
    }
    public function loadById(Request $r,$resource,$id)
    {
        $model=$this->makeModel($resource);
        $instance= $model::findOrFail($id);
        if($instance)
            return response()->json($instance);
        return response()->json(['message'=>'not found'],404);
    }

    public function patch(Request $r,$resource,$id)
    {
        \Log::info("Patch request zahtjev: ",[(array)$r->all(),'id'=>$id]);
        $model= $this->makeModel($resource);


        $instance= $model::where('id',$id)->first();

        if(!$instance->customUpdate($r,$id))
            return response()->json(['message'=> 'Something wen\'t wrong'],404);

        if(!$instance->update($r->all()))
            return response()->json(['message'=>'Something wen\'t wrong'],404);

        if(!$instance->postUpdate($r, $id))
            return response()->json(['message'=>'Something wen\'t wrong'],404);

        return response()->json(['message'=>'Successifully updated'],200);
        //  If update failed
        return response()->json(['message'=>'Something wen\'t wrong'],404);

    }
    public function userStatus(Request $r)
    {
        $user=(\Auth::user())?\Auth::user():JWTAuth::parseToken()->authenticate();
        return $user->status();
    }
    public function loadHive($apiarId)
    {
        return BaseModel::loadHives($apiarId);
    }
    public function destroy(Request $r,$resource,$id)
    {
        // dd($id);
        \Log::info("Delete:",(array)$r->all());
        $model= $this->makeModel($resource);
        $instance= $model::where('id',$id)->first();
        if($instance->del())
            return response()->json(['message'=>'succesifully deleted']);
        return response()->json(['message'=>'not found'],404);
    }

    public function communicate(Request $r)
    {
        \Log::info('Request',(array)$r->all());
        if(isset($r['I']))
        {
            $imei=$r['I'];
            $long_lat=$r['L'];
            $long_lat_arr= \explode(',',$long_lat);
            $longitude= $long_lat_arr[1];
            $latitude= $long_lat_arr[0];
            $amplitude= $r['Vamp'];
            $addr= $r['Addr'];
            $data=[
                'longitude'=> $longitude,
                'latitude'=> $latitude,
                'IMEI'=> $imei,
                'Vamp'=> $amplitude,
                'Addr'=> $addr,
                'BatCU'=> $r['BatCU'],
                'List'=> $r['List']
            ];

            $hive= Hive::where('imei','=',$data['IMEI'])
                ->where('address','=',$data['Addr'])
                ->first();
            \Log::info('Hive: ',(array)$hive);
            if($r->has('List'))
            {
                $hive= Hive::where('imei','=',$data['IMEI'])->first();
                if($hive!==null)
                    $hive->processAddressList($r);
            }else {
//            if($hive!==null && $hive->isLicenseActive())

                $cu= Hive::where('imei','=',$data['IMEI'])
                                ->where('unit_id','=',1)
                                ->where('address','=',$data['Addr'])
                                ->orWhere('address','=','null')
                                ->first();
                if($cu!==null)
                    $cu->processCuData($r, $data);
                else if($hive!==null)
                    $hive->processHiveData($data);
            }
            \Log::info("Podaci",$data);
            http_response_code(200);
            return response()->json(['message'=>'success']);
        }else {
            http_response_code(404);
            return response()->json(['message'=>'not found'], 404);
        }
        http_response_code(404);
        return response()->json(['message'=>'not found'], 404);
    }
    public function notifyMovedHives(Request $r)
    {
        if(!isset($r['ids']))
            return response()->json(['message'=>'not found'],404);
        $success=Hive::notifyUsersForMovedHives($r['ids']);
        if($success)
            return response()->json(['message'=>'success'],200);
        return response()->json(['message'=>'not found'],404);
    }

}
