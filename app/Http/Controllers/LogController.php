<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function list(Request $r)
    {
        $yesterday= Carbon::yesterday();
        $data= \App\LogModel::orderBy('created_at','desc')
                                ->where('created_at','>',$yesterday)
                                ->get();
        foreach($data as $row)
        {
            if(isset($row->request))
                $row->request= \json_decode($row->request);

            if(isset($row->hive))
                $row->hive= \json_decode($row->hive);

            if(isset($row->report_request))
                $row->report_request= \json_decode($row->report_request);

            if(isset($row->report_unit))
                $row->report_unit= \json_decode($row->report_unit);

        }
        return view('Test/log',['log' => $data]);
    }
}
