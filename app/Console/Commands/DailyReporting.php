<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DailyReporting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running unit daily report check';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Daily cron working');
        foreach (\App\Hive::all() as $hive) {
            $hive->checkDailyReport();
        }
        // dd('Jedinica se javila');
    }
}

