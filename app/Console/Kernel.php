<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Hive;
use App\Jobs;
use App\User;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $includeDefaultCommands = false; 
    protected $commands = [
        Commands\create_db::class,
        Commands\DailyReporting::class
    ];

    // private $hive;
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            //Log::info('Env vrijednost TEST_MODE:'.env('TEST_MODE'));
            if(env('TEST_MODE')=='false' || env('TEST_MODE')==0)
            {
                // Log::info('Usao u ovaj mod');
                foreach(\App\Hive::all() as $hive)
                {
//                $hive->checkHiveMovedStatus();
                }
                foreach (User::all() as $user) {
                    $user->checkLicenseExpired();
                }
            }

        })->appendOutputTo(public_path('cron.log'));
        $now= \Carbon\Carbon::now();
        // Log::info("Worker radi ".$now->diffForHumans());
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
            $this->load(__DIR__.'/Commands');
            require base_path('routes/console.php');
    }
}
