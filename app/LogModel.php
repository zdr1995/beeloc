<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogModel extends Model
{
    protected $table= 'log_reporting';
    protected $primary_key='id';
    public $timestamps= true;
    protected $fillable=[
        'request',
        'hive',
        'type',
        'report_request',
        'report_unit'
    ];
}
