<?php

namespace App;

use LaravelFCM\Facades\FCM;
use Carbon\Traits\Timestamp;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use LaravelFCM\Message\OptionsBuilder;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // protected $created_at=false;
    // protected $updated_at=false;
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'expire',
        'status',
        'address',
        'access_token',
        'remember_token',
        'fcm_token',
        'role',
        'created_at',
        'updated_at',
        'phone_number',
        'ready_for_listen',
        'notified_in'
    ];
    protected $appends = [
        'unit_number'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at'=> 'datetime',
        'updated_at'=> 'datetime'
    ];

    protected $rules = [
        'name' => 'required|string',

    ];

    protected $guarded = ['created_at', 'updated_at'];
    protected static $licence_expired_message= 'Postovani, vasa licenca istice za %s dana';
    protected static $licence_expired_title= 'Obavestenje o isteku licence';
    public function __construct()
    {
        parent::boot();
        $this->created_at= time();
        $this->updated_at= time();
    }
    public function apiars()
    {
        return $this->hasMany('\App\Apiar','user_id');
    }

    public function hives($query=false)
    {
        $apiars= $this->apiars();
        $hives=collect();
        // dd($apiars);
        foreach($apiars->get() as $apiary) {
            $hives->push($apiary->hives);
        }
        return ($query)?$hives:$hives->toBase();
    }
    public function withRelations()
    {
        $this->hives=$this->hives(false);
        return $this;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getUnitNumberAttribute()
    {
        $query="
            SELECT count(hives.id) as unit_number
            FROM apiars 
            LEFT JOIN hives
                ON hives.apiar_id= apiars.id
            WHERE apiars.user_id= $this->id
        ";
        return DB::SELECT(DB::raw($query))[0]->unit_number;
    }

    public function getLoggedIn()
    {
        try{
            $user=(\Auth::user())?\Auth::user():\JWTAuth::user();
            return $user;
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return false;
        }
    }

    protected function makePsswd(Request $r)
    {
        $this->password = Hash::make($r->password);
    }

    public function create(Request $r)
    {
        $user=User::where('email','=',$r->email)->get();
        if(count($user)==0)
        {
            $user=new User($r->all());
            $user->makePsswd($r);
            if($user->save())
                return true;
            return false;
        }else{
            return false;
        }

    }
    public function login(Request $r)
    {
        // dd($r->all());
        Log::info("Login zahtjev: ",$r->all());
        $addr=$_SERVER['REMOTE_ADDR'];
        Log::info("Address: $addr");
        $rules=[
            'email'  => 'required|string',
            'password'  => 'required|string|min:6|max:12'
        ];
        $validator=Validator::make($r->only(['email','password']), $rules);
        // dd($validator->fails());
        if($validator->fails())
            response()->json(['message'=>'invalid creditials']);

        $data=[
            'email' => $r->email,
            'password' => $r->password
        ];
        // dd($data);
        $user=\App\User::where('email',$data['email'])->first();
        // dd($user);
        if($user!=null)
        {
            $data['id']=$user->id;
            // dd($data);
            try{
                $token=\JWTAuth::attempt($r->only('email','password'));
                if($token){
                    // app('auth')->setUser($data);
                    \Log::info("Ulogovan user u login: ",(array)\Auth::user());
                    $user->token=$token;
                    return response()->json($user,200);
                }
            }catch(\Tymon\JWTAuth\Exceptions\JWTException $e) {
                dd($e);
                return response()->json(['message'=>'not found'],404);
            }


            return response()->json(['message'=>'not found'],404);
        }else return response()->json(['message'=>'Wrong username or password']);

    }

    public function loginWeb(Request $r)
    {

        $rules=[
            'username'  => 'required|string',
            'password'  => 'required|string|min:6|max:12'
        ];
        $validator=Validator::make($r->only(['username','password']), $rules);

        if($validator->fails())
            response()->json(['message'=>'invalid creditials']);

        $data=$r->only('email','password');


        $user=User::where('email',$data['email']);
        if( $user=\Auth::attempt($data))
        {
            // dd(\Auth::user());
            Log::info('User ulogovan',['user'=>\JWTAuth::user()]);
            return $user;
        }else return false;
    }


    public function loadMain()
    {
        $main=collect();
        // dd($this->hives(false));
        if(count($this->hives(false))){
            $hives=$this->hives(false);
            foreach($hives as $hive){
                foreach($hive as $unit) {
                    // dd($unit->main);
                    if($unit->main)
                    $main->push($unit);
                }

            }//->where('main','=','1');
            // dd();
            return response()->json($main,200);
        }else return null;

    }
    public function status()
    {
        // dd($this);
        $apiars_num=$this->apiars->count();
        $hive_count= 0;

        $licence['days']= Carbon::now()->diffInDays(($this->expire));
        ($licence['days']<30)?$licence['alarm']=1:$licence['alarm']=0;

        // if($this->expire)
        $query= "
            SELECT 
                units.name,
                count(hives.id) as quantity,
                units.type AS type 
            FROM hives LEFT JOIN units 
                ON hives.unit_id=units.id 
            WHERE hives.apiar_id IN(
                        SELECT apiars.id FROM apiars 
                        WHERE apiars.user_id = $this->id
                    ) 
            GROUP BY units.name,units.type
        ";  
        $q=\DB::select(\DB::raw($query));
        $hive_quantity=0;
        foreach($q as $hive)
        {
            $hive_quantity+=$hive->quantity;
        }
        $arr=[
            'Alarm'   => $licence['alarm'],
            'Apiaries'  =>$apiars_num,
            'hives'     =>$hive_quantity,
            'Units'     =>$q
        ];
        return $arr;

    }
    public function loadAll()
    {
        $users= \App\User::all();
        foreach($users as $user) {
            $user->number=$user->status();
        }
        return response()->json($users);
    }
    public function patch(Request $r)
    {
        $instance=$this;
        if($instance->update($r->all()))
            return true;
        else return false;
    }
    public function updateFCM(Request $r)
    {
        if($this->update(['fcm_token'=>$r->fcm_token]))
            return true;
        return false;
    }
    public function sendNotification(array $data, $id)
    {
        $token= User::where('id','=',$id)->first()->fcm_token;
        // dd($token);
        Log::info('User send notification',(array)($token));
        if($token!==null)
        {
            if($data['type']=='licence')
            {
                $data['extra']= 'open_licence';
                $data['click_action']='OPEN_LICENCE';
            }else {
                $data['extra']=0;
                $data['click_action']=0;
            }
            try{
                \fcm()->to([$token])
                ->priority('high')
                // ->timeToTive(0)
                ->data([
                    'info'=> $data['title'],
                    'message'=>$data['message'],
                    'sound'=>'notification.mp3'
                ])->notification([
                    'body'=>$data['message'],
                    'title'=> $data['title'],
                    'open_click'=> $data['click_action'],
                    'extra'=> $data['extra'],
                    'sound'=>'notification.mp3'
                ])->send();
                return true;
            }catch (\Exception $e){
                Log::info("Error u notification send: ",(array)($e));
            }
        }else {
            return false;
        }

    }
    public static function nearExpiredLicenses()
    {
        $now=time();
        // dd($now);
        $difference= 30;
        $query="
        SELECT
            id,
            name,
            email,
            expire,
            status,
            DATEDIFF(users.expire,NOW())as diff
        FROM users
        GROUP BY
            id,
            name,
            email,
            expire,
            status,
            diff
        HAVING diff<$difference AND diff>0
            ";
        $users= DB::select(DB::raw($query));
        return $users;
    }
    public function isActiveLicense()
    {
        if(time()>$this->expire)
            return true;
        return false;
    }
    public function isAdmin()
    {
        return $this->role=='admin';
    }
    public function sendHiveMovedNotification(\App\Hive $hive)
    {
        try{
            $id= \DB::table('apiars')->where('id','=',$hive->apiar_id)->first()->user_id;
            \Log::error("User_id za notifikaciju: $id");
            if($id==null)
                throw new \Exception('Cannot read user_id');
            $data=[
                'message'=> $hive->name.' je pomerena',
                'title'=> 'Došlo je do pomeranja košnice ',
                'type'=> 'moving'
            ];
            $this->sendNotification($data,$id);
        }catch(\Exception $e) {
            Log::error('Exception: ',(array)$e);
        }

    }
    public function patchListenStatus(Request $r)
    {
        $user= $this->getLoggedIn();
        if($r['isActive']==1)
        {
            sleep(4);
            $user->sendHivePosition();
        }
        if($user==null)
            return false;
        Log::info((array)$r['isActive']);
        $user->ready_for_listen= $r['isActive'];
//        dd($user->save());
        if(!$user->save() )
            return false;

        return true;
    }

    public function sendHivePosition()
    {
        $data=[];
        $hives= $this->hives(true);
        foreach($hives as $apiary_hive)
        {
            foreach ($apiary_hive as $hive)
            {
                // Different than potera unit
                if($hive->unit_id!=4)
                {
                    $data[]=[
                        'id'=> $hive->id,
                        'last_reporting'=> $hive->last_reporting,
                        'name'=> $hive->name,
                        'battery'=> $hive->battery,
                        'longitude'=> $hive->longitude,
                        'latitude'=> $hive->latitude,
                        'moved'=> $hive->moved
                    ];
                }
            }
        }
        $all_data['hives']=$data;
        $all_data['user']= $this->id;
//        sleep(2);
        event(new \App\Events\LocationEvent(($all_data)));
    }
    public function checkLicenseExpired()
    {
        $now= Carbon::now();
        $expires= new Carbon($this->expire);
        // \Log::info(['sada'=>(array)$now,'istice'=>(array)$expires]);
        $days_difference= $expires->diffInDays($now);
        if($days_difference>30)
        {
            $this->status=0;
            $this->save();
        }else if($days_difference==15 && $this->notified_in!==15)
        {
            $data=[
                'message'=> sprintf(static::$licence_expired_message,15),
                'title'=> static::$licence_expired_title,
                'type'=> 'licence'
            ];
            $this->sendNotification($data, $this->id);
            $this->notified_in= 15;
        }else if($days_difference==7 && $this->notified_in!==7)
        {
            $data=[
                'message'=> sprintf(static::$licence_expired_message,7),
                'title'=> static::$licence_expired_title,
                'type'=> 'licence'
            ];
            $this->sendNotification($data, $this->id);
            $this->notified_in=7;
        }else if($days_difference==3 && $this->notified_in!==3)
        {
            $data=[
                'message'=> sprintf(static::$licence_expired_message,3),
                'title'=> static::$licence_expired_title,
                'type'=> 'licence'
            ];
            $this->sendNotification($data, $this->id);
            $this->notified_in=3;
        }
        // $this->expire= $now->addDays(4);
        $this->save();
    }
}
