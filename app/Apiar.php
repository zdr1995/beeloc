<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Apiar extends BaseModel
{
    protected $table = 'apiars';
    protected $created_at = false;
    protected $updated_at = false;
    const CREATED_AT = false;
    const UPDATED_AT = false;
    //  15 Minutes
    protected static $timeDelay= 60*60*15;

    protected $primaryKey='id';
    public $incrementing=true;
    public $fillable=[
        'id',
        'name',
        'user_id',
        'sleep',
        'change'
    ];
    public $guarded=[];

    public $timestamps=false;
    // protected $rules=[
    //     'name'      => 'required|string',
    //     // 'user_id'   => 'required|exists:user'
    // ];
    protected static $rules= [
        'name'=>'required:string',
        'user_id'=>'required'
    ];

    public function validate($data)
    {
        $validator= Validator::make($data, static::$rules);
        if($validator->fails())
            return false;
        else return true;
    }


    public function setUserNameAttribute()
    {
        dd($this->id);
        $user=\App\User::where('id','=',$this->id)->first();
        dd($user);
        // return $this->attributes['user_name']=$this->users[0]->name;
    }
    protected $mutate=[
        'user_name' => 'apiar_name'
    ];

    // relations
    public function hives()
    {
        return $this->hasMany('\App\Hive','apiar_id');
        // return $this->hasMany('\App\Hive');
    }
    public function mainCU()
    {
        // dd($this->hives()->where('main','=',1)->get(['id']));
        return $this->hives()->where('main','=',1)->first();
    }
    public function users()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }
    public function  getFullName()
    {
        return $this->name;
    }
    public function del()
    {

        $hives=$this->hives;
        foreach($hives as $hive)
            $hive->delete();
        if($this->delete())
            return true;
        return false;
    }

    public function insertMainCU(Request $r)
    {
        if(!$r->has('unit_name') || !$r->has('unit_imei'))
            return false;
        $hive_cu= new Hive();
        $hive_cu->name= $r['unit_name'];
        $hive_cu->imei= $r['unit_imei'];
        $hive_cu->address= 10;
        $hive_cu->unit_id= 1;
        $hive_cu->main=1;
        $hive_cu->last_reporting= Carbon::now();
        $hive_cu->apiar_id= $this->id;
        if($hive_cu->save())
            return true;
        return false;
    }
    public function edit(Request $r)
    {
        $this->fill($r->all());
        $this->save();
    }

    /**
     * @override
     */
    public function customUpdate(Request $r, $id)
    {
         if($r->has('change'))
         {
//             dd($this->hives);
             foreach($this->hives as $hive)
             {
                $hive->moved=0;
//                $hive->reporting= 0;
                $hive->save();
             }
//             $query= "UPDATE hives SET moved=0 WHERE apiar_id=$this->id";
//             DB::raw($query);
         }
        return true;
    }


}
