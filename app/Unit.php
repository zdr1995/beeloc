<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends BaseModel
{
    protected $table='units';
    protected $fillable=[
        'id',
        'name',
        'type'  
    ];
    protected $rules=[
        'name' => 'required|string',
        'type' => 'required|string|max:30'
    ];
    protected $hidden = ['id'];

    public function hives()
    {
        return $this->belongsTo('\App\Hive');
    }
    public function addAttributes($data=[],$type) 
    {
        foreach($data as $attr=>$value) {
                $this->attributes[$type]=$value;
        }
    }
}
