<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Apiar::class, function (Faker $faker) {
    $randId=\App\User::inRandomOrder()->first()->id;
    // dd($randId);
    return [
        'name' => $faker->name,
        'user_id' => $randId
    ];
   
});
