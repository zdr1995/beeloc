<?php

use Illuminate\Database\Seeder;

class UnitHiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hives=\App\Hive::all();
        foreach($hives as $hive)
        {
            for($x=0;$x<3;$x++)
            {
                $faker=\Faker\Factory::create();
                DB::table('unit_hive')->insert([
                    'imei'      => $faker->macAddress,
                    'longitude' => rand(0.1,56.00),
                    'latitude'  => rand(0.1,56.00),
                    'battery'   => rand(0,100),
                    'main'      => array_rand([0,1],1),
                    'hive_id'   => $hive->id,
                    'unit_id'   => \App\Unit::InRandomOrder()->first()->id
                ]);
            }
        }
    }
}
