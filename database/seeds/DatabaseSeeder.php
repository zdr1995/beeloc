<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        // $this->call(UsersTableSeeder::class);
        // $this->call(ApiarTableSeeder::class);
        // $this->call(UnitTableSeeder::class);
        $this->call(HiveTableSeeder::class);

        

        // $this->call(UnitHiveTableSeeder::class);
        // Model::reguard();
    }
}
