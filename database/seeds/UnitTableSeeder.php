<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected  static $names=[
        'Central Unit',
        'Sensor Tag',
        'Sensor Tag + GPS',
        'Potera Unit'
    ];

    protected static $types=[
            'CU',
            'ST',
            'ST+GPS',
            'PU'
        ];


    public function run()
    {
        for($x=0;$x<4;$x++)
        {
            $faker=\Faker\Factory::create();
            DB::table('units')->insert([
                'name'    => static::$names[$x],
                'type'    => static::$types[$x]
            ]);
        }
    }
}
