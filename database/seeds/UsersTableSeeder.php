<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $userData=[
            'name' => 'Djordje Petro',
            'email' => 'djordje.petrovic@itcentar.rs',
            'email_verified_at' => now(),
            'password' => Hash::make('djordje123'),
            'expire' => \Carbon\Carbon::now()->addYear()->addMonths(3),
            'status' => array_rand([true,false],1),
            'remember_token' => Str::random(10),
            'access_token' => Str::random(25)
        ];
        $user=\App\User::firstOrCreate($userData);
        $userData=[
            'name' => 'Zdravko Sokcevic',
            'email' => 'zdravko.sokcevic@itcentar.rs',
            'email_verified_at' => now(),
            'password' => Hash::make('zdravko123'),
            'expire' => \Carbon\Carbon::now()->addYear()->addMonths(3),
            'status' => array_rand([true,false],1),
            'remember_token' => Str::random(10),
            'access_token' => Str::random(25)
        ];
        $user=\App\User::firstOrCreate($userData);
        // $user->save();
        factory(\App\User::class,55)->create()->each(function($u){
           
            // $u->apiar()->save(factory(\App\Apiar::class,1)->create());
        });


        // factory(
    }
}
