<?php

use Illuminate\Database\Seeder;

class ApiarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId=\App\User::inRandomOrder()->first()->id;
        // dd($userId);
        if( count(\App\Apiar::all() )==0) {
        $appiar=[
            [
                'name'  => 'Pcelinjak 1',
                'user_id'  => '1'
            ],[
                'name'  => 'Pcelinjak djoletov',
                'user_id' => $userId
            ],[
                'name'  => 'Pcelinjak Zdravkov',
                'user_id'  => '1'
            ],[
                'name'  => 'Pcelinjak milosev',
                'user_id' => $userId
            ],[
                'name'  => 'Pcelinjak pajin',
                'user_id'  => '1'
            ],[
                'name'  => 'Pcelinjak no. 1',
                'user_id' => $userId
            ]
        ];
            $appiars=\DB::table('apiars')->insert($appiar);
        }

        $app = \App\User::all();
        foreach($app as $user)
        {
            $faker=\Faker\Factory::create();
            DB::table('apiars')->insert([
                'name'      => $faker->name,
                'user_id'   => $user['id'],
                'sleep'     => array_rand([0,1],1),
                'change'    => array_rand([0,1],1)
            ]);
        }

        // factory(\App\Apiar::class,10)->make();
        // $appiars->save();
    }
}
