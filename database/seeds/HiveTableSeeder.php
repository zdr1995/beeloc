<?php

use Illuminate\Database\Seeder;

class HiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $apiars = \App\Apiar::all();


        \DB::table('hives')->insert([
            'name'          => 'Trstenik',
            'longitude'     => 43.62071622078891,
            'latitude'      => 20.99624054469095,
            'last_reporting'=> date('Y-m-d'),
            'reporting'     => array_rand([0,1],1),
            'apiar_id'      => 1,
            'main'          => 0,
            'imei'          => \Faker\Factory::create()->imei,
            'address'       => '12846641168',
            'battery'       => rand(0,100),
            'notification'  => array_rand([0,1],1),
            'unit_id' => \App\Unit::inRandomOrder()->first()->id
        ]);


        foreach($apiars as $apiar)
        {
            $faker=\Faker\Factory::create();
            \DB::table('hives')->insert([
                'name'          => $faker->name,
                'longitude'     => 43.6251970656641,
                'latitude'      => 20.89513800262705,
                'last_reporting'=> date('Y-m-d'),
                'reporting'     => array_rand([0,1],1),
                'apiar_id'      => $apiar->id,
                'main'          => array_rand([0,1],1),
                'imei'          => \Faker\Factory::create()->imei,
                'address'       => '12846641168',
                'battery'       => rand(0,100),
                'notification'  => array_rand([0,1],1),
                'unit_id' => \App\Unit::inRandomOrder()->first()->id
            ]);
        }
    }
}
