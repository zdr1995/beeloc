<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_reporting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('request')->nullable();
            $table->text('hive')->nullable();
            $table->string('type');
            $table->string('report_request')->nullable();
            $table->string('report_unit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_reporting');
    }
}
