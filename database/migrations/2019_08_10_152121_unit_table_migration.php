<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnitTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units',function(Blueprint $table){
            $table->engine='InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->enum('type', ['ST','CU','ST+GPS','PU']);
            // $table->bigInteger('hive_id')->unsigned();
        });
        // Schema::table('units',function(Blueprint $table){
        //     $table->foreign('hive_id')->references('id')->on('hives');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('units');

    }
}
