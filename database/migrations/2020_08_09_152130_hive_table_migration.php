<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HiveTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hives',function(Blueprint $table){
            $table->engine='InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->string('reporting')->nullable();
            $table->string('main');
            $table->string('battery')->nullable();
            $table->tinyInteger('moved')->nullable();
            $table->tinyInteger('notification_sent')->nullable();
            $table->date('last_reporting')->nullable();
            $table->string('imei')->nullable();
            $table->bigInteger('apiar_id')->unsigned();
            $table->bigInteger('unit_id')->unsigned();
            $table->string('address',50);
        });
        Schema::table('hives',function(Blueprint $table){
            $table->foreign('apiar_id')->references('id')->on('apiars')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('hive');
        
    }
}
