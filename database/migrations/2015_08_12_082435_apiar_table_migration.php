<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiarTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apiars',function(Blueprint $table){
            $table->engine='InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name',50);
            $table->integer('sleep')->default(0);
            $table->integer('change')->default(0);
            $table->bigInteger('user_id')->unsigned()->nullable();
        });
        Schema::table('apiars',function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('apiars');

    }
}
