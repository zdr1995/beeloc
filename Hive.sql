-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2019 at 01:28 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Hive`
--

-- --------------------------------------------------------

--
-- Table structure for table `apiars`
--

CREATE TABLE `apiars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sleep` int(11) NOT NULL DEFAULT '0',
  `change` int(11) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apiars`
--

INSERT INTO `apiars` (`id`, `name`, `sleep`, `change`, `user_id`) VALUES
(1, 'Broj jedan', 1, 0, 1),
(2, 'Pcelinjak djoletov', 0, 0, 32),
(3, 'Pčelinjak 2', 1, 0, 5),
(4, 'Pcelinjak milosev', 0, 0, 32),
(5, 'Pcelinjak pajin', 1, 0, 2),
(6, 'Pcelinjak no. 1', 0, 0, 32),
(7, 'Pcelinjak 1', 1, 1, 3),
(8, 'Pcelinjak 2', 1, 0, 3),
(10, 'Dayne Pollich', 0, 0, 4),
(11, 'Carissa Green', 0, 0, 5),
(12, 'Mr. Korey Bechtelar V', 1, 1, 6),
(13, 'Mozell Langworth', 1, 1, 7),
(14, 'Webster Littel', 0, 1, 8),
(15, 'Dr. Brycen Ziemann IV', 0, 0, 9),
(16, 'Garnet Wilkinson DVM', 0, 1, 10),
(17, 'Phoebe Prosacco', 0, 1, 11),
(18, 'Alva McKenzie', 1, 1, 12),
(19, 'Prof. Casimir Maggio Sr.', 0, 0, 13),
(20, 'Presley Dach', 0, 0, 14),
(21, 'Israel Aufderhar', 0, 0, 15),
(22, 'Mr. Bradley Kreiger', 1, 1, 16),
(23, 'Eleonore Paucek', 0, 1, 17),
(24, 'Ms. Zita Lebsack', 0, 0, 18),
(25, 'Carlotta Lehner Jr.', 0, 0, 19),
(26, 'Nona Kovacek', 1, 0, 20),
(27, 'Idella Cremin', 1, 1, 21),
(28, 'Julie Baumbach', 0, 0, 22),
(29, 'Xavier Nikolaus', 1, 0, 23),
(30, 'Eric Bailey', 1, 0, 24),
(31, 'Mr. Dashawn Bradtke Jr.', 0, 0, 25),
(32, 'Lonzo Torp', 0, 1, 26),
(33, 'Pascale Hettinger IV', 1, 1, 27),
(34, 'Jody Grant III', 1, 1, 28),
(35, 'Dr. Brendan Mohr', 1, 1, 29),
(36, 'Mrs. Kirsten Strosin', 0, 1, 30),
(37, 'Ms. Maryjane Zboncak MD', 1, 0, 31),
(38, 'Angelita Dare', 1, 1, 32),
(39, 'Patrick Marks MD', 1, 1, 33),
(40, 'Ms. Veda Huels', 0, 1, 34),
(41, 'Adell Mayert', 1, 0, 35),
(42, 'Prof. Sedrick Kuphal II', 1, 0, 36),
(43, 'Prof. Jaden Lesch', 0, 1, 37),
(44, 'Keshaun Morissette', 1, 1, 38),
(45, 'Lon Ledner PhD', 1, 1, 39),
(46, 'Mrs. Angie Keebler Jr.', 1, 1, 40),
(47, 'Hope Crooks', 1, 0, 41),
(48, 'Ciara Kertzmann', 0, 0, 42),
(49, 'Nestor Abbott', 1, 0, 43),
(50, 'Ewald Hessel', 1, 1, 44),
(51, 'Axel Little III', 1, 0, 45),
(52, 'Jada Koch', 1, 1, 46),
(53, 'Doyle Bahringer', 0, 1, 47),
(54, 'Llewellyn Barrows', 0, 0, 48),
(55, 'Alexzander Hartmann', 1, 0, 49),
(56, 'Vicky Corwin', 0, 1, 50),
(57, 'Tristian Torp', 1, 1, 51),
(58, 'Kirstin Heidenreich V', 1, 1, 52),
(59, 'Miss Margarett Sauer Jr.', 0, 1, 53),
(60, 'Linnea Willms', 1, 1, 54),
(61, 'Kenya Huel V', 1, 0, 55),
(62, 'Alexie Romaguera', 0, 1, 56),
(63, 'Ms. Lelah Daugherty DDS', 0, 0, 57);

-- --------------------------------------------------------

--
-- Table structure for table `hives`
--

CREATE TABLE `hives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `reporting` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `battery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` int(11) NOT NULL,
  `last_reporting` date NOT NULL,
  `imei` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apiar_id` bigint(20) UNSIGNED NOT NULL,
  `unit_id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hives`
--

INSERT INTO `hives` (`id`, `name`, `longitude`, `latitude`, `reporting`, `main`, `battery`, `notification`, `last_reporting`, `imei`, `apiar_id`, `unit_id`, `address`) VALUES
(1, 'Glavna kosnica', 43.62071622078891, 20.99624054469095, '1', '0', '26', 1, '2019-08-28', '861694033985837', 1, 1, '10'),
(2, 'Glavna CU', 43.6251970656641, 20.89513800262705, '0', '1', '17', 0, '2019-08-28', '698328807944154', 2, 1, '12846641168'),
(3, 'Zdravko herbiko', 43.6251970656641, 20.89513800262705, '0', '0', '3', 0, '2019-08-28', '495682667945445', 2, 3, '12846641168'),
(4, 'Novo', 43.6251970656641, 20.89513800262705, '0', '0', '70', 1, '2019-08-28', '262896637837059', 3, 2, '12846641168'),
(5, 'Orville Marquardt MD', 43.6251970656641, 20.89513800262705, '0', '0', '88', 1, '2019-08-28', '774406839660100', 4, 1, '12846641168'),
(6, 'Lenny Kutch Sr.', 43.6251970656641, 20.89513800262705, '1', '1', '80', 0, '2019-08-28', '609040187413969', 5, 4, '12846641168'),
(7, 'Emilia Koch', 43.6251970656641, 20.89513800262705, '0', '1', '34', 1, '2019-08-28', '616523505056686', 6, 4, '12846641168'),
(8, 'Centralna jedinica', 21.926722689067617, 42.54730363019271, '0', '1', '75', 1, '2019-08-28', '616523505056684', 7, 1, '10'),
(9, 'Sensor Tag', 43.6251970656641, 20.89513800262705, '0', '0', '86', 0, '2019-08-28', '6', 7, 2, '10'),
(10, 'Sensor Tag + GPS', 43.6251970656641, 20.89513800262705, '1', '0', '91', 1, '2019-08-28', '7', 7, 3, '10'),
(11, 'Central Unit 1', 21.71095111880416, 43.53991946788234, '1', '1', '80', 1, '2019-08-28', '724380962590920', 8, 1, '10'),
(12, 'Sensor Tag', 43.6251970656641, 20.89513800262705, '1', '0', '33', 1, '2019-08-28', '4', 8, 2, '5'),
(13, 'ST + GPS', 43.6251970656641, 20.89513800262705, '1', '0', '61', 0, '2019-08-28', '8', 8, 3, '12846641168'),
(14, 'Potera Unit', 20.89513800262705, 43.6251970656641, '1', '0', '68', 1, '2019-08-28', '399229957088234', 8, 4, '12846641168'),
(15, 'Earnestine Hills', 43.6251970656641, 20.89513800262705, '0', '1', '57', 1, '2019-08-28', '724380962790920', 14, 3, '12846641168'),
(16, 'Dr. Keith Cole', 43.6251970656641, 20.89513800262705, '0', '1', '55', 0, '2019-08-28', '107428938790388', 15, 3, '12846641168'),
(17, 'Kendall Crona', 43.6251970656641, 20.89513800262705, '1', '0', '51', 0, '2019-08-28', '628673583535568', 16, 4, '12846641168'),
(18, 'Dr. Ben Green DVM', 43.6251970656641, 20.89513800262705, '0', '1', '9', 0, '2019-08-28', '673183613627579', 17, 3, '12846641168'),
(19, 'Shaina Harber', 43.6251970656641, 20.89513800262705, '1', '0', '24', 1, '2019-08-28', '705541684639660', 18, 2, '12846641168'),
(20, 'Brenna Collier IV', 43.6251970656641, 20.89513800262705, '0', '1', '87', 1, '2019-08-28', '765069629556457', 19, 1, '12846641168'),
(21, 'ST2', 43.6251970656641, 20.89513800262705, '1', '1', '89', 1, '2019-08-28', '3', 1, 2, '10'),
(22, 'Potera Unit', 43.6251970656641, 20.89513800262705, '0', '1', '87', 1, '2019-08-28', '', 1, 3, '10'),
(23, 'ST+GPS2', 43.6251970656641, 20.89513800262705, '0', '0', '98', 0, '2019-08-28', '1', 1, 3, '10'),
(24, 'ST+GPS1', 43.6251970656641, 20.89513800262705, '1', '0', '36', 1, '2019-08-28', '5', 1, 3, '10'),
(25, 'Hiive', 43.6251970656641, 20.89513800262705, '1', '1', '88', 0, '2019-08-28', '4', 1, 2, '10'),
(26, 'Narciso Dach', 43.6251970656641, 20.89513800262705, '1', '1', '46', 0, '2019-08-28', '003888851492715', 25, 1, '12846641168'),
(27, 'Vincent Gulgowski', 43.6251970656641, 20.89513800262705, '0', '1', '91', 1, '2019-08-28', '285471505517135', 26, 2, '12846641168'),
(28, 'Parker McCullough', 43.6251970656641, 20.89513800262705, '0', '0', '95', 0, '2019-08-28', '342003672012532', 27, 4, '12846641168'),
(29, 'Mallie Brekke', 43.6251970656641, 20.89513800262705, '0', '0', '41', 1, '2019-08-28', '471183136640269', 28, 1, '12846641168'),
(30, 'Tyler Kuhlman', 43.6251970656641, 20.89513800262705, '0', '1', '3', 1, '2019-08-28', '811746050626032', 29, 2, '12846641168'),
(31, 'Sadie Lynch', 43.6251970656641, 20.89513800262705, '0', '0', '41', 0, '2019-08-28', '439555242223620', 30, 2, '12846641168'),
(32, 'Dr. Matilde Jenkins', 43.6251970656641, 20.89513800262705, '1', '1', '72', 0, '2019-08-28', '027270225713955', 31, 1, '12846641168'),
(33, 'Prof. Tremaine Sauer', 43.6251970656641, 20.89513800262705, '1', '0', '51', 1, '2019-08-28', '136043731408370', 32, 1, '12846641168'),
(34, 'Aniyah Wolff MD', 43.6251970656641, 20.89513800262705, '1', '1', '49', 0, '2019-08-28', '052352923872867', 33, 3, '12846641168'),
(35, 'Donny Price', 43.6251970656641, 20.89513800262705, '0', '1', '30', 0, '2019-08-28', '318106382530329', 34, 2, '12846641168'),
(36, 'Terrance Becker DDS', 43.6251970656641, 20.89513800262705, '0', '1', '10', 1, '2019-08-28', '277858059716966', 35, 4, '12846641168'),
(37, 'Dr. Enrique Schmitt III', 43.6251970656641, 20.89513800262705, '0', '0', '22', 1, '2019-08-28', '651136451276756', 36, 3, '12846641168'),
(38, 'Maxie Marquardt', 43.6251970656641, 20.89513800262705, '0', '1', '53', 0, '2019-08-28', '089323157119405', 37, 3, '12846641168'),
(39, 'Dr. Margot Ernser', 43.6251970656641, 20.89513800262705, '1', '1', '89', 1, '2019-08-28', '596861719951811', 38, 3, '12846641168'),
(40, 'Dr. Rhett Roberts', 43.6251970656641, 20.89513800262705, '1', '0', '45', 0, '2019-08-28', '039239076882736', 39, 1, '12846641168'),
(41, 'Jovan Dare', 43.6251970656641, 20.89513800262705, '1', '0', '74', 1, '2019-08-28', '111254220504194', 40, 4, '12846641168'),
(42, 'Baby Stanton', 43.6251970656641, 20.89513800262705, '0', '0', '20', 1, '2019-08-28', '229631143203658', 41, 2, '12846641168'),
(43, 'Dr. Hannah Rosenbaum', 43.6251970656641, 20.89513800262705, '0', '1', '47', 0, '2019-08-28', '459678306342626', 42, 3, '12846641168'),
(44, 'Prof. Scarlett Torphy', 43.6251970656641, 20.89513800262705, '1', '0', '29', 0, '2019-08-28', '494296453269752', 43, 3, '12846641168'),
(45, 'Mr. Keeley Spencer II', 43.6251970656641, 20.89513800262705, '1', '1', '91', 0, '2019-08-28', '268023740898142', 44, 4, '12846641168'),
(46, 'Patrick Murray', 43.6251970656641, 20.89513800262705, '0', '0', '49', 0, '2019-08-28', '631345186024101', 45, 3, '12846641168'),
(47, 'Cynthia Frami', 43.6251970656641, 20.89513800262705, '0', '0', '91', 1, '2019-08-28', '113156538066121', 46, 3, '12846641168'),
(48, 'Mrs. Serena Von I', 43.6251970656641, 20.89513800262705, '0', '1', '72', 1, '2019-08-28', '820097575332386', 47, 4, '12846641168'),
(49, 'Furman Kuhlman', 43.6251970656641, 20.89513800262705, '1', '1', '60', 1, '2019-08-28', '633617011784168', 48, 4, '12846641168'),
(50, 'Reinhold Oberbrunner IV', 43.6251970656641, 20.89513800262705, '0', '1', '90', 1, '2019-08-28', '201835006117606', 49, 3, '12846641168'),
(51, 'Rodger Schuppe DVM', 43.6251970656641, 20.89513800262705, '0', '1', '82', 0, '2019-08-28', '353152338906524', 50, 1, '12846641168'),
(52, 'Kirsten Upton PhD', 43.6251970656641, 20.89513800262705, '1', '0', '95', 1, '2019-08-28', '006099017230943', 51, 3, '12846641168'),
(53, 'Anne Kshlerin', 43.6251970656641, 20.89513800262705, '0', '1', '16', 1, '2019-08-28', '722647887222581', 52, 4, '12846641168'),
(54, 'Mariela Leannon', 43.6251970656641, 20.89513800262705, '1', '1', '86', 1, '2019-08-28', '879207586911892', 53, 1, '12846641168'),
(55, 'Maximilian Stamm', 43.6251970656641, 20.89513800262705, '1', '1', '60', 1, '2019-08-28', '972394639809582', 54, 2, '12846641168'),
(56, 'Arno Prosacco DDS', 43.6251970656641, 20.89513800262705, '1', '1', '14', 1, '2019-08-28', '082027433356841', 55, 3, '12846641168'),
(57, 'Carrie Moen Sr.', 43.6251970656641, 20.89513800262705, '0', '0', '52', 1, '2019-08-28', '113780025627242', 56, 1, '12846641168'),
(58, 'Damaris Schmidt', 43.6251970656641, 20.89513800262705, '0', '1', '54', 0, '2019-08-28', '355777716697614', 57, 3, '12846641168'),
(59, 'Demarco Padberg', 43.6251970656641, 20.89513800262705, '1', '1', '61', 1, '2019-08-28', '222456987261712', 58, 3, '12846641168'),
(60, 'Paolo Leuschke Jr.', 43.6251970656641, 20.89513800262705, '0', '1', '94', 0, '2019-08-28', '593115577539900', 59, 3, '12846641168'),
(61, 'Ms. Alverta Crona PhD', 43.6251970656641, 20.89513800262705, '0', '0', '47', 1, '2019-08-28', '596971716529427', 60, 2, '12846641168'),
(62, 'Gordon Beer', 43.6251970656641, 20.89513800262705, '1', '1', '96', 0, '2019-08-28', '790027718086090', 61, 3, '12846641168'),
(63, 'Prof. Piper Langworth DDS', 43.6251970656641, 20.89513800262705, '1', '1', '80', 1, '2019-08-28', '538725355550735', 62, 3, '12846641168'),
(64, 'Kelli Williamson', 43.6251970656641, 20.89513800262705, '1', '0', '20', 0, '2019-08-28', '782301166542452', 63, 1, '12846641168');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_08_12_082435_apiar_table_migration', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(7, '2016_06_01_000004_create_oauth_clients_table', 1),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2019_08_10_152121_unit_table_migration', 1),
(10, '2020_08_09_152130_hive_table_migration', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('ST','CU','ST+GPS','PU') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `type`) VALUES
(1, 'Central Unit', 'CU'),
(2, 'Sensor Tag', 'ST'),
(3, 'Sensor Tag + GPS', 'ST+GPS'),
(4, 'Potera Unit', 'PU');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `expire`, `status`, `remember_token`, `access_token`, `created_at`, `updated_at`, `fcm_token`) VALUES
(1, 'Đorđe Petrović', 'djordje.petrovic@itcentar.rs', '2019-08-28 10:44:03', '$2y$10$DVXt9sg94VJgIRJvfXBvQ.IvGz7M0sbvrVikXdiRpy7gSaxpm26vG', '2020-11-28', 0, 'vLR4xzYZZBy327OOS6028OGcowhc2ZAJ86mU9kF5NALKkFHBV48g7P5zdso7', 'QDg4gVB24csWcAUXFRy7ufm3F', NULL, '2019-08-28 13:01:49', NULL),
(2, 'Zdravko Sokcevic', 'zdravko.sokcevic@itcentar.rs', '2019-08-28 10:44:04', '$2y$10$XTTldD6AGGrZ6l3zTnP3zOoIw3.CmxTPjBLLRHlSef3ZMcCz0fO1a', '2020-11-28', 1, 'mrYpbfLeOz', 'l8w3iDnXEeIyUdE42mZZvPTmn', NULL, '2019-09-05 13:49:24', NULL),
(3, 'Test Korisnik', 'test@test.com', '2019-08-28 10:44:04', '$2y$12$SLg/zAMzptqe8yWmjqne9OxtheWhp3hxadtjZCZmp.MkghVJfk7wO', '2020-11-28', 1, 'FRxdv1uNhrPuPn0PVyVPXPeXGAoNEF1Z4OhFrh5t6agFoM2HMclgCES3LS2Z', 'VuX2Gzoyg2eY4hlhbn40EkF68', NULL, '2019-09-06 07:06:56', 'e-ciU5LQVk4:APA91bHu-6djiDYpQPjhJ66tWsrB8NrllM_yDyRiLvpLhPkbF3lSTS3-tNpBPz1uU5geFkwNcTwJbdCUa_-9wuGK8W9PRPEHy8VOrCOZ-J5EhwYdx8ljEtOBuZTiez5g_vv3npmYn7su'),
(4, 'Angelina Gutkowski', 'wisozk.lola@example.net', '2019-08-28 10:44:04', '$2y$10$HR/YKEoek8iLyAjzo910iuq9BlmqUFF/75/7PqqQ6o4aU4mH9Ke6C', '2020-11-28', 1, 'l5rLqnprb7', 'msoSI4VICJEXdublRaaqR8XP5', NULL, NULL, NULL),
(5, 'Wilford Parker', 'mitchell.juanita@example.org', '2019-08-28 10:44:04', '$2y$10$e6Ftto3Nmm9gEg0rud776eVW4lBWaUHZ15qa2NzgG3t1eNmi47cMK', '2020-11-28', 1, 'VMhK8ZUKDy', 'kAAbz9geIlLg1kex58GVQmFcQ', NULL, NULL, NULL),
(6, 'Ruth Ernser', 'bailey20@example.com', '2019-08-28 10:44:04', '$2y$10$DPmPdt.LA5dEXDziokVM8.jwItU8ZqK9QNe8KGzEWpqFE8CBma9Ym', '2020-11-28', 1, 'Nr7bFqHBpM', 'Xa1ra9go1aOdJI22OKoGRrkgN', NULL, NULL, NULL),
(7, 'Prof. Kiana Treutel I', 'block.taurean@example.com', '2019-08-28 10:44:04', '$2y$10$SNLTd1G.1CHOkT0Fl1oezO9/.mqBNVhGNUhTJ4x/y6pg70Qhvpqm2', '2020-11-28', 1, 'iC0rwHObE0', '2XL7w6co4kY9sxxT68pJ1OccO', NULL, NULL, NULL),
(8, 'Sarina Mertz', 'wilkinson.maudie@example.net', '2019-08-28 10:44:04', '$2y$10$Rd8H9qTaQYAV90IYwnvxZuugOd6jDVhLbqPqp8CGLoUPAVXccePL6', '2020-11-28', 0, 'YsD8frXGFQ', '3vXuYDNytMXpRkRijC2JbSf9T', NULL, NULL, NULL),
(9, 'Camila Wyman', 'ervin29@example.net', '2019-08-28 10:44:04', '$2y$10$7cg80NFA4AH0EKloAYMcce8nP.c5KFYsUJlg.Hbh6O0ng6e0QqmE6', '2020-11-28', 1, 'g7zqibj2LN', 'XaoSJEHKgIOldUQoS6KZfbH1z', NULL, NULL, NULL),
(10, 'Edgar Gulgowski', 'lindsey.veum@example.com', '2019-08-28 10:44:04', '$2y$10$0rHqm8mqVpOdnkyWSO.2BuX1KeIBaSpe27xGa9Y/JtLllr0qSPykm', '2020-11-28', 1, 'n8chGYjdeY', 'PKh3Mjm6aKw2FTv1w7KN774Nr', NULL, NULL, NULL),
(11, 'Weston Collier', 'ybartoletti@example.org', '2019-08-28 10:44:04', '$2y$10$i8toyoFmlDWgJR/iTnYI8euZNmscEc5VVXnlj.5zBG9LGQr4Jxt62', '2020-11-28', 1, 'lFQ09ygazw', 'HcNdvDntsN9zsTNCxylG9dugv', NULL, NULL, NULL),
(12, 'Gerda Cummings', 'sprohaska@example.org', '2019-08-28 10:44:04', '$2y$10$bfWlOXR9vz2LNnokK2tqje/yP4.ossK8J9cUMjVLuEYSl.Z9FDtEe', '2020-11-28', 1, 'kWjpK2dbAE', 'lhnPdmgXft83juqcLeAcOXxg2', NULL, NULL, NULL),
(13, 'Mr. Randy Lind', 'vhowe@example.net', '2019-08-28 10:44:04', '$2y$10$VIsqlCrYOsr1nN5yAPSLweJYiokjeWZVjjiL.GrE37XihMq0fXb9S', '2020-11-28', 0, 'K4LNFD7c2X', 'SxVLrReAQH4VGhVrpqdz4hulW', NULL, NULL, NULL),
(14, 'Johnpaul Simonis', 'johnathan.lindgren@example.com', '2019-08-28 10:44:04', '$2y$10$jrIr5jqmT7UygYd54DVLh.Nd2d6Xhx5fYUd/aAglH.ELQyHIwVXBa', '2020-11-28', 1, 'zwYSsPaTYp', 'q82l12Smdo56xRwcnQJup48R2', NULL, NULL, NULL),
(15, 'Alexandra Weimann', 'catalina51@example.net', '2019-08-28 10:44:04', '$2y$10$JxyqHGq7H43xPWseFzKQFOXPIVEJvzKkK43kB6gYWSb3EiE2XfD5i', '2020-11-28', 0, 'KrOA51vjC4', 'IdVJevXj7hKx7qhvnEjYEqkce', NULL, NULL, NULL),
(16, 'Miss Caleigh Ward Sr.', 'dorothy.monahan@example.net', '2019-08-28 10:44:04', '$2y$10$yxS3HBXYP0TyfaQ/wjhb7.wsFszl2sd81NbjL.uJiYNBORsRDB2bG', '2020-11-28', 1, 'wI3On9EVw3', '5TfngLyoEFcLHcDymSD3GTsfW', NULL, NULL, NULL),
(17, 'Santina Block', 'djacobs@example.org', '2019-08-28 10:44:05', '$2y$10$VP5MHAX7CB3acOO7TYGUqe2SelLnkYmWO9BLhThazMeZUIQGFHuKm', '2020-11-28', 0, 'yDQPkvCtdj', 'oF2S3XleIwsrQjVnwHfyhjKii', NULL, NULL, NULL),
(18, 'Dr. Izaiah Kshlerin DDS', 'pfeil@example.net', '2019-08-28 10:44:05', '$2y$10$F7qAqRURsVTJciKe2yGxjOUG8GbohpIPtWqmwmd4j5PGqPhukgRp6', '2020-11-28', 1, 'f1wP0IdRpc', '2wEXxyNXZpLv39xYzFFecW1YT', NULL, NULL, NULL),
(19, 'Gonzalo Krajcik DDS', 'hohara@example.net', '2019-08-28 10:44:05', '$2y$10$yFbQv44CDHCWzxWGC3/A6OgeIz5J1PJ7c92Yo42fh2k2H6K2eUj2O', '2020-11-28', 0, 'VkAZTTVQoJ', 'ZSTRYt3DIWA5bj6A91xfFi2zz', NULL, NULL, NULL),
(20, 'Elton Abernathy', 'edgardo.cummings@example.net', '2019-08-28 10:44:05', '$2y$10$kZTqx/POLSm8sW5KXz0gbOld2U0ZqAX783N6ji1M4OTVzcihijJza', '2020-11-28', 0, 'BOjQFbtRPb', 'vKFet5qcPnPOuxTceVklqOwsJ', NULL, NULL, NULL),
(21, 'Josiah Stark', 'amy42@example.net', '2019-08-28 10:44:05', '$2y$10$.2eS949c0DdLmSd7zg/nL./oPC6/0ZcBoHQalue8XAA4BnnjIbHNi', '2020-11-28', 1, '8mf1tpHfQr', 'EyGy4SX41zEYktyr13lSmASNz', NULL, NULL, NULL),
(22, 'Prof. Alexandra Russel', 'pearl23@example.org', '2019-08-28 10:44:05', '$2y$10$P/P7Q1xDEfbZBgrM6oRCtOaRqVEjxu7OqukDb8PniSQQgVqLefEKi', '2020-11-28', 1, 'HNWCEXHHw5', 'VdpKQ6llWrQN1eY2P60xYB1ma', NULL, NULL, NULL),
(23, 'Hugh Rogahn', 'catharine27@example.net', '2019-08-28 10:44:05', '$2y$10$YJD6KxVJ2Vq2Ac/xt.Xe3u1PpYP6wC5/okucRYxYeqLUkhSqHTIK.', '2020-11-28', 0, 'rHrCuWrLay', '3icxl4ZR2nQLM09xT66rM4kZn', NULL, NULL, NULL),
(24, 'Lucienne Abshire Jr.', 'schuyler.reichert@example.net', '2019-08-28 10:44:05', '$2y$10$rsZWDHDPp2v0k5mNPH6Ti.DETcUeXOdl6RJPgsSlDA1T4cyeX/65e', '2020-11-28', 0, 'SgUl35xyIn', 'e8v8eaBxFxapUhBxfyV74yK4i', NULL, NULL, NULL),
(25, 'Mr. Wiley Eichmann II', 'nettie67@example.org', '2019-08-28 10:44:05', '$2y$10$1xItHVU2H6Y64jFlE7/uKeepaXIAQTnuabj13LAvbX6UFwMpr6SkG', '2020-11-28', 1, 'QANkXFlodT', '8Ads2nM9guj6MMmot4vQFQJjT', NULL, NULL, NULL),
(26, 'Prof. Vallie Graham', 'colby.haag@example.com', '2019-08-28 10:44:05', '$2y$10$6HMT/r/FGi1GfnsYgRdByeIV7IhiHWd1sfvq5v5brL5M/QC0tYSHy', '2020-11-28', 1, 'ACwHNCceVS', '56JeFLsiNULxUK1uwENgBjXGe', NULL, NULL, NULL),
(27, 'Mireya Swaniawski', 'dannie37@example.net', '2019-08-28 10:44:05', '$2y$10$WIVJha8tT6EYtcza6C/IyOQF4ekGPnyCCPPPd/515jd5pQ3fNFiAW', '2020-11-28', 1, 'kXGbAybXeT', '3e8rE06tLSzaBom87xl7yilPd', NULL, NULL, NULL),
(28, 'Candice Kling', 'lehner.tad@example.org', '2019-08-28 10:44:05', '$2y$10$HwwixZdRSsEPHhN7.mava.A6jU/nlOo4xeSvArEpBWk6Zp8E3k5Qm', '2020-11-28', 0, 'vEQlLIWvQU', 'R6OQFkxM0uyCIKUQquppyqGRF', NULL, NULL, NULL),
(29, 'Trace Schuster', 'skihn@example.org', '2019-08-28 10:44:05', '$2y$10$rqX90.h5/Uueu7Aicc0hDeq5rQWdCV3zO5Z/LkuIqQTVvpajIdToC', '2020-11-28', 0, 'vRig6l0b1L', 'TcQCCHml90YO5b39dsayNhAPO', NULL, NULL, NULL),
(30, 'Phyllis Olson', 'qhaley@example.org', '2019-08-28 10:44:05', '$2y$10$lM1eivmw.j6Gd6zR3cDGeOwKtO0uOL2GMXHqKg/Vj62NXb267fCSa', '2020-11-28', 0, 'ubPN1VGu6A', '9f9Cc1d4sIVW4VFSsepbyrhPh', NULL, NULL, NULL),
(31, 'Patsy Weissnat', 'ondricka.vance@example.com', '2019-08-28 10:44:05', '$2y$10$6D8VRqOuNIUq.dAMSol/8u285c4ZY/cSXjqX2VnERhtVtdeEiU.6e', '2020-11-28', 1, 'QAv7aMh330', 'ThwB6E6Jo8SMAONAJHknwoDTy', NULL, NULL, NULL),
(32, 'Mauricio Roberts', 'frussel@example.net', '2019-08-28 10:44:05', '$2y$10$sEwh5ueUnIrffsGhiFMbPeSuyaW6aUWf0yY013nE6IRcXpCCwFN0m', '2020-11-28', 1, 'd9xHQyiiGt', 'RwnTBOur4kJHOA4rUTOB3WjXo', NULL, NULL, NULL),
(33, 'Miss Missouri Ondricka', 'jaquelin.kautzer@example.org', '2019-08-28 10:44:05', '$2y$10$BMqbyqH.nP.SNpF91ahFleN6I2rhH49c1MHQJHNv2amID/FYY9Izi', '2020-11-28', 0, '4VTxRDaEav', 'MSRaqkBQQoiPlRXY579uVlQ3x', NULL, NULL, NULL),
(34, 'Rubie Jacobs', 'mohr.isobel@example.com', '2019-08-28 10:44:06', '$2y$10$sSIvVLIzISO0J9o5tc/U9OOBlxhMOZZApNNJjiTwT0ST8.4ZqvT5u', '2020-11-28', 1, '0SzuunejW0', '86ol7lmCQtS6I7cOq9QUQ3LJM', NULL, NULL, NULL),
(35, 'Velva Waelchi III', 'ebeer@example.org', '2019-08-28 10:44:06', '$2y$10$nhUIDu19UBQDGBbEsCzVB.gdcHFpxZbUmjFx24yfjDBoL6MoHKBhy', '2020-11-28', 1, '6L7pRyXrfV', 'oHxoHvO1dDpleW7Nm6xe1uzRs', NULL, NULL, NULL),
(36, 'Orion Littel', 'felicia67@example.com', '2019-08-28 10:44:06', '$2y$10$40quST9lFhF9rrysPsXMC.6wBpQFn5SCSa403BsDOxhaIjS1dUhh6', '2020-11-28', 0, 'fIIyrCrxAX', 'huQxIgKiCuMHzMTgxcWRZLdOK', NULL, NULL, NULL),
(37, 'Dr. Mattie Cole DDS', 'emerald69@example.com', '2019-08-28 10:44:06', '$2y$10$Q7zI0vCSNYx21Cfe.pWrFOkBn3Nm212IaqP1DJbZACm5kwBgF08i2', '2020-11-28', 0, 'ThU3pgLrWA', 'oYgIkC8p0t38Bt4ZnEF2HgdV9', NULL, NULL, NULL),
(38, 'Prof. Jaunita Quigley', 'gnienow@example.org', '2019-08-28 10:44:06', '$2y$10$dVL/I5z/TGkkl1TPnC7ea.LfKrTJ/TCjE.9iJIRe/hccuOZD7Fq7y', '2020-11-28', 0, 'K9Egjopd3v', 'vmWTJEVxL8ZPThz5nU5lHEo50', NULL, NULL, NULL),
(39, 'Johnny Fadel', 'elroy.vonrueden@example.net', '2019-08-28 10:44:06', '$2y$10$/z8X8XgbNUpUtk/dMvwA6uNgXtT.nf3Y5KrhnzMjbvm5LYByY0O/C', '2020-11-28', 0, '4nnunBix3k', 'YwHK0pRn8rhyJABMlnWkEbAc5', NULL, NULL, NULL),
(40, 'Sylvan Watsica', 'dpaucek@example.org', '2019-08-28 10:44:06', '$2y$10$Nze9rJ.15nszx3zndPaj6eooR8SfDvb9sn8KXKXR29jGPog5vFRQW', '2020-11-28', 1, 'IAg73WUzNQ', 'dLNZ1CeHvn1Vj1tNjLMmwyAqR', NULL, NULL, NULL),
(41, 'Rico Farrell', 'austen.dietrich@example.com', '2019-08-28 10:44:06', '$2y$10$fRl/8lQvUwhapigMnvOTceJg9eyglvtRjqwlsbZr2etzxVMH/x4yW', '2020-11-28', 1, 'OiYc8HWCIo', '5KiEeA2HOgbubWtk2AN0F48cA', NULL, NULL, NULL),
(42, 'Jacklyn Funk', 'oberbrunner.trevor@example.net', '2019-08-28 10:44:06', '$2y$10$M6vbB4PMxqV3pCpBGfpNi.3h8M9VKyViQM4VtGysmiBFKpkZVYSq2', '2020-11-28', 0, 'jKyMt7M1fU', 'dhzisfzUXdtMstnWY9kMCyefh', NULL, NULL, NULL),
(43, 'Carey Corkery DVM', 'wwelch@example.com', '2019-08-28 10:44:06', '$2y$10$86Y24Oz5GuJZAr79ACFC/eGQkm.YK28DAJfK0iUz521Eh.cuvJlrG', '2020-11-28', 0, 'Vnw6EbHzkO', '0FqVSnlzaREMWA5v9gB8IBxji', NULL, NULL, NULL),
(44, 'Aaliyah Yost Jr.', 'trisha.hagenes@example.org', '2019-08-28 10:44:06', '$2y$10$FOy75le4ifHZLx56SFIL..IRyJZ.0Fk86jwsXlNQonhUO8qxDAu/K', '2020-11-28', 0, 'Li7lh4TaHy', 'Ejq9yCBNKPanj5axo4aqzoowB', NULL, NULL, NULL),
(45, 'Cullen Breitenberg', 'goodwin.mustafa@example.org', '2019-08-28 10:44:06', '$2y$10$vVO07fjDXPPZVVFbqmQ3quUXBNYjlgJtp/fx25i6/YhMsj1/hsd2y', '2020-11-28', 1, 'r49NJNfdjt', 'di1jSNHBYIVI1TVZSdPvP3IMJ', NULL, NULL, NULL),
(46, 'Zelma Senger', 'leilani.dietrich@example.org', '2019-08-28 10:44:06', '$2y$10$83j9t6463PnuhlbvBFevgef4P3va5d2mUlIXnoU5knXFDoHAcqZOK', '2020-11-28', 0, 'OBVQpWOQIQ', 'cfpCZj3ePYpwS6qkj0vCrAOx2', NULL, NULL, NULL),
(47, 'Bella Kovacek', 'bednar.alfonso@example.org', '2019-08-28 10:44:06', '$2y$10$4QJ53IEtpCran4V5uFj6Ces1/RyLy80MCDjMgz5enqpXa2UkpZGFm', '2020-11-28', 0, 'i8inmK3chq', 'za7odvArvdNkySbTwwu9s1DHp', NULL, NULL, NULL),
(48, 'Milo Mante', 'terry.miller@example.com', '2019-08-28 10:44:06', '$2y$10$OJ9lDdwRnQ4U3GCi9mg24uEmBeAfB7pu0r3k3KKnkFBUptEOrTKLa', '2020-11-28', 0, 'RNUAoDY1RE', 'vpfZ5WgoZp1giHlCPCLhwtmhw', NULL, NULL, NULL),
(49, 'Prof. Lia Balistreri IV', 'lilian.hills@example.net', '2019-08-28 10:44:06', '$2y$10$xxnjFXicEkOHTFOsbsEG5u0Xec/RaTBEYhknIVKjzy74eygG8Pjm.', '2020-11-28', 1, 'wU1o3d540x', 'POSg8KVtyl0gu7mfunonsSJwQ', NULL, NULL, NULL),
(50, 'Mr. Stone Klocko', 'marilie99@example.net', '2019-08-28 10:44:06', '$2y$10$oF0XscfHgVZ9TEnUYv0PnuDFE9wm4IlSn2CqEZRdN5wWLPLSG3Pu.', '2020-11-28', 1, '81eX0HJnkK', 'QpgjBrFIOmzUpB7ow0BLG3iKY', NULL, NULL, NULL),
(51, 'Dr. Hollie Jones', 'cartwright.myrtis@example.org', '2019-08-28 10:44:07', '$2y$10$d3W8upT6bfanzs.qBvNfauWYxU7azW3p.tC5XYbs/HJX5Oz8Gol6O', '2020-11-28', 0, 'IT4pGeg4oW', 'TUxvCVFK2znDcTd4Kpod6LCbp', NULL, NULL, NULL),
(52, 'Cecile Cassin Sr.', 'hahn.marty@example.com', '2019-08-28 10:44:07', '$2y$10$x.sNw7YWdNDyWUA1DRWp/.mQHCaphW3jwU2iCgsP0AckfEzkfEEHe', '2020-11-28', 1, '33hs4mrirm', 'PGZgpZ0n8cubQQosDx4Z2KF29', NULL, NULL, NULL),
(53, 'Yolanda Bayer', 'unicolas@example.org', '2019-08-28 10:44:07', '$2y$10$2DiHtE61Eb3Y5R7tnVbEyeHXu8aXCXd6SlCy.i.BbILVCG.GvDCpi', '2020-11-28', 1, 'J5UPg5IVTh', 'cpCxfAFw4wXZi7sfvR0qs3HYQ', NULL, NULL, NULL),
(54, 'Myrtle Wolff I', 'jannie.schultz@example.org', '2019-08-28 10:44:07', '$2y$10$FgO78jZL7HJTb9POI9wv0uplpzrQFb0m3jdlca0lxF1Pcg.eGqqFG', '2020-11-28', 0, 'FYQPrRHRZp', 'go3viDsn0litOj142N21e8GCZ', NULL, NULL, NULL),
(55, 'Jackeline Blanda', 'taurean34@example.com', '2019-08-28 10:44:07', '$2y$10$gViHvS7kxzKouZzKWn7vIuDoerEmwS44pFsy0bcGRJsg8W74NpsO.', '2020-11-28', 1, 'nZyGGskLkR', 'p3RHR3ylpoeNZXcpOcd3TNoMx', NULL, NULL, NULL),
(56, 'Mrs. Mina Schumm', 'ova44@example.com', '2019-08-28 10:44:07', '$2y$10$3sbVvn0oifel7j.vdUjR9OXkUvYycaiVSVyo3E8UAKnHqQ5OzlvvK', '2020-11-28', 1, 'QtwzGU2Skp', 'Yook5zTUYIXIdm8T6RL0zFa19', NULL, NULL, NULL),
(57, 'Sidney Welch', 'mariane.satterfield@example.net', '2019-08-28 10:44:07', '$2y$10$kA92yVXu1ySc968TR5RrJu40GEV9gQk1rov5.c5iwSmF4lbJo7S9C', '2020-11-28', 1, 'tMsS8SfEQX', 'VvPnfzzpy9ZUaHAkyhKNnFZeo', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apiars`
--
ALTER TABLE `apiars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `apiars_user_id_foreign` (`user_id`);

--
-- Indexes for table `hives`
--
ALTER TABLE `hives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hives_apiar_id_foreign` (`apiar_id`),
  ADD KEY `hives_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apiars`
--
ALTER TABLE `apiars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `hives`
--
ALTER TABLE `hives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `apiars`
--
ALTER TABLE `apiars`
  ADD CONSTRAINT `apiars_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `hives`
--
ALTER TABLE `hives`
  ADD CONSTRAINT `hives_apiar_id_foreign` FOREIGN KEY (`apiar_id`) REFERENCES `apiars` (`id`),
  ADD CONSTRAINT `hives_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
